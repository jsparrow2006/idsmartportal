﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
using WebSite.Models;

namespace WebSite.Controllers
{
    public class PrincipalController : Controller
    {
        //
        // GET: /Principal/

        public ActionResult Index()
        {
            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

            if (user == null)
                return PartialView("Error");

            ViewBag.User = user.fullname;
            return View();
        }

        // Get: /Principal/Schedule
        [HttpGet]
        public ActionResult Schedule()
        {
            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
            ViewBag.Test = "";
            if (user == null)
                return PartialView("Error");
            ViewBag.Klass = WebApiConfig2.WebClient.ПолучитьКлассы(Convert.ToInt32(user.SchoolID)).ToList();
            ViewBag.Visible = "hidden";
            return View();
        }

        // Post: /Principal/Schedule
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Schedule(string submit)
        {
            //Array ScheduleArr = new Array[4];
            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
            if (user == null)
                return PartialView("Error");
            ViewBag.Klass = WebApiConfig2.WebClient.ПолучитьКлассы(Convert.ToInt32(user.SchoolID)).ToList();
            var less = WebApiConfig2.WebClient.ПолучитьПредметыКласса(Convert.ToInt32(submit)).ToArray();
            var lessons = from t in less
                select t.Наименование;
            ViewBag.LessonsList = lessons.ToArray();

            var Shedules = WebApiConfig2.WebClient.ПолучитьРасписание(Convert.ToInt32(submit)).ToList();

            var q = from t in Shedules
                where t.ДеньНедели.Наименование == "Понедельник"
                select t.Предмет.Наименование;
            ViewBag.d0 = q.ToArray();
            q = from t in Shedules
                where t.ДеньНедели.Наименование == "Вторник"
                select t.Предмет.Наименование;
            ViewBag.d1 = q.ToArray();
            q = from t in Shedules
                where t.ДеньНедели.Наименование == "Среда"
                select t.Предмет.Наименование;
            ViewBag.d2 = q.ToArray();
            q = from t in Shedules
                where t.ДеньНедели.Наименование == "Четверг"
                select t.Предмет.Наименование;
            ViewBag.d3 = q.ToArray();
            q = from t in Shedules
                where t.ДеньНедели.Наименование == "Пятница"
                select t.Предмет.Наименование;
            ViewBag.d4 = q.ToArray();
            q = from t in Shedules
                where t.ДеньНедели.Наименование == "Суббота"
                select t.Предмет.Наименование;
            ViewBag.d5 = q.ToArray();

            ViewBag.Test = submit;
            ViewBag.Visible = "visible";
            return View("Schedule");
        }

    }
}
