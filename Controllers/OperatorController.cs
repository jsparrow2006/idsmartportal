﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using WebSite.Filters;
using WebSite.Models;
using Newtonsoft.Json;

namespace WebSite.Controllers
{
    [Authorize(Roles = "admins, managers, operator, teacher")]
    [InitializeSimpleMembership]
    public class OperatorController : Controller
    {
        //
        // GET: /Operator/
        public ActionResult Index()
        {
            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

            if (user == null)
                return PartialView("Error");

            ViewBag.User = user.fullname;

            return View();
        }
            // GET: /Operator/Journal
            public ActionResult Journal(string school)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
                /*
                for (int i = Schools.Count - 1; i >= 0; i--)
                {
                    if (Schools[i].Код != Int32.Parse(user.SchoolID))
                    { Schools.RemoveAt(i); }
                }
                */
                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }                    
                    }
                }
            }
            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №8 от журнала
                if (Schools[i].Код < 2)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.Klass = "";
            ViewBag.School = (school == null) ? "" : school;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Journal
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Journal(string school, int unit_id, string klass, int subject, string quarter)
        {
            string error_str = "";

            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
            int school_id = 0;

            foreach (var item in Schools)
            {
                if (item.Наименование == school)
                { school_id = item.Код; }
            }

            var Quarters = new List<WebServiceReference2.Четверть_Строка>();

            try
            {
                Quarters = WebApiConfig2.WebClient.ПолучитьЧетверти(school_id).ToList();
            }
            catch (System.ServiceModel.FaultException ex)
            {
                error_str += ex.Message;
                ViewBag.Pupil = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "SchoolID: " + school_id;
                error.Command = "ПолучитьЧетверти " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            /*
            List<Quarter> Quarters = null;
            if (school == "Гимназия N1" || school == "Гимназия N2")
            {
                Quarters = new List<Quarter>
                {
                    new Quarter {ID = 0, Name = "1й триместр", Date1 = "01.09.2015", Date2 = "30.11.2015" },
                    new Quarter {ID = 1, Name = "2й триместр", Date1 = "01.12.2015", Date2 = "29.02.2016" },
                    new Quarter {ID = 2, Name = "3й триместр", Date1 = "01.03.2016", Date2 = "30.05.2016" },
                };
            }
            else
            {
                Quarters = new List<Quarter>
                {
                    new Quarter {ID = 0, Name = "1-я четверть", Date1 = "01.09.2015", Date2 = "25.10.2015" },
                    new Quarter {ID = 1, Name = "2-я четверть", Date1 = "02.11.2015", Date2 = "27.12.2015" },
                    new Quarter {ID = 2, Name = "3-я четверть", Date1 = "11.01.2016", Date2 = "20.03.2016" },
                    new Quarter {ID = 3, Name = "4-я четверть", Date1 = "28.03.2016", Date2 = "31.05.2016" },
                };
            }
            */
            ViewBag.Quarter = Quarters;
            /*
            if (quarter == "")
            {
                if (DateTime.Now.CompareTo
                    (DateTime.ParseExact(Quarters[1].Date1, "dd.MM.yyyy", CultureInfo.InvariantCulture)) <= 0)
                { quarter = "0"; }
                else if (DateTime.Now.CompareTo
                    (DateTime.ParseExact(Quarters[2].Date1, "dd.MM.yyyy", CultureInfo.InvariantCulture)) <= 0)
                { quarter = "1"; }
                else if (Quarters.Count > 3)
                {
                    if (DateTime.Now.CompareTo(DateTime.ParseExact(Quarters[3].Date1, "dd.MM.yyyy", CultureInfo.InvariantCulture)) <= 0)
                    { quarter = "2"; }
                    else
                    { quarter = "3"; }
                }
                else
                { quarter = "2"; }
            }
            */
            int quarter_id = 0;
            List<DateTime> Days = new List<DateTime>();
            var Months = new List<Month>();

            if (Quarters.Count > 0)
            {
                if (quarter == null)
                {
                    if (DateTime.Now.CompareTo(Quarters[1].НачалоЧетверти) <= 0)
                    { quarter = "1"; }
                    else if (DateTime.Now.CompareTo(Quarters[2].НачалоЧетверти) <= 0)
                    { quarter = "2"; }
                    else if (Quarters.Count > 3)
                    {
                        if (DateTime.Now.CompareTo(Quarters[3].НачалоЧетверти) <= 0)
                        { quarter = "3"; }
                        else
                        { quarter = "4"; }
                    }
                    else
                    { quarter = "3"; }
                }

                quarter_id = Int32.Parse(quarter);
                ViewBag.QuarterSel = quarter;
                
                var BeginDay = Quarters[quarter_id - 1].НачалоЧетверти;
                var EndDay = Quarters[quarter_id - 1].КонецЧетверти;
                //var BeginDay = DateTime.ParseExact(Quarters[quarter_id].Date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                //var EndDay = DateTime.ParseExact(Quarters[quarter_id].Date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                var start_date = DateTime.Now.AddDays(7);

                for (int i = 333; i >= 0; i--)
                {
                    var sub = TimeSpan.FromDays(i);
                    DateTime day = start_date.Subtract(sub);
                    if (DateTime.Compare(day, BeginDay) >= 0 && day.DayOfWeek != DayOfWeek.Sunday && day.DayOfWeek != DayOfWeek.Saturday)   // заменить на проверку по расписанию
                    { Days.Add(day); }
                    if (DateTime.Compare(day, EndDay) >= 0)
                    { break; }
                }
            }
            else
            { error_str += "Список четвертей пустой. "; }

            var Subjects = new List<WebServiceReference2.Предмет_Строка>();
            try
            {
                Subjects = WebApiConfig2.WebClient.ПолучитьПредметыКласса(unit_id).ToList();
                Subjects.Sort((x, y) =>
                {
                    return x.Наименование.CompareTo(y.Наименование);
                });
                ViewBag.Subjects = Subjects;
                if (Subjects.Count == 0)
                { error_str += "Пустой список предметов. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.Subjects = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьПредметыКласса " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            var TimeTables = new List<WebServiceReference2.Расписание_Строка>();
            try
            {
                TimeTables = WebApiConfig2.WebClient.ПолучитьРасписание(unit_id).ToList();
                ViewBag.TT = TimeTables;
                if (TimeTables.Count == 0)
                { error_str += "Расписание пустое. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.TimeTables = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьРасписание " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            for (int i = Days.Count - 1; i >= 0; i--)
            {
                string day_str = WeekDay(Days[i]);
                int flag = 0;
                foreach (var item in TimeTables)
                {
                    if (item.Предмет.Код == subject && item.ДеньНедели.Наименование == day_str)
                    { flag++; }
                }
                if (flag == 0)
                { Days.RemoveAt(i); }
            }

            var Pupils = new List<WebServiceReference2.Ученик_Строка>();
            try
            {
                Pupils = WebApiConfig2.WebClient.ПолучитьУчеников(unit_id).ToList();
                Pupils.Sort((x, y) =>
                {
                    if (x.ОплаченыОценки != y.ОплаченыОценки)
                    {
                        if(x.ОплаченыОценки == true)
                        { return -1; }
                        else
                        { return 1; }
                    }

                    return x.Наименование.CompareTo(y.Наименование);
                });
                ViewBag.Pupils = Pupils;
                if (Pupils.Count == 0)
                { error_str += "Список учеников пустой. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.Pupils = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьУчеников " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            if (Pupils.Count > 0 && Days.Count > 0)
            {
                var Marks_arr = new List<List<WebServiceReference2.Оценка_Строка>>();
                
                foreach (var pupil in Pupils)
                {
                    try
                    {
                        var Marks = WebApiConfig2.WebClient.ПолучитьОценкуУченикаПоПредметуЗаПериод(pupil.Код, Days.First(), Days.Last(), subject);
                        Marks_arr.Add(Marks.ToList());
                    }
                    catch (System.ServiceModel.FaultException ex)
                    {
                        ViewBag.Error = ex.Message;
                        ViewBag.Marks = null;

                        var DB = new DataContext();
                        var error = new Error();
                        string str = "subject: " + subject.ToString() + " pupil: " + pupil.Код.ToString() +
                           " date1: " + Days.First().ToString("dd.MM.yyyy") + " date2: " + Days.Last().ToString("dd.MM.yyyy");
                        error.Command = "ПолучитьОценкуУченикаПоПредметуЗаПериод " + str;
                        error.Text = ex.Message;
                        error.Date = DateTime.Now;
                        DB.Errors.Add(error);
                        DB.SaveChanges();
                    }
                }
                ViewBag.Marks = Marks_arr;

                string month = Days[0].ToString("MMMM");
                int span = 0;
                foreach (var day in Days)
                {
                    var m = day.ToString("MMMM");
                    if (m != month)
                    {
                        Months.Add(new Month { span = span, name = month });
                        month = m;
                        span = 0;
                    }
                    span++;
                }
                Months.Add(new Month { span = span, name = month });
                
                var Fresh_arr = new List<List<int>>();
                int a = 0;
                foreach (var pupil in Pupils)
                {
                    var Fresh = new List<int>();
                    int b = 0;
                    foreach (var day in Days)
                    {
                        int flag = 0; 

                        foreach (var mark in Marks_arr[a])
                        {
                            if (day.ToString("dd.MM.yyyy") == mark.Период.ToString("dd.MM.yyyy"))
                            {
                                if (mark.ДатаСохранения > DateTime.Now.Subtract(TimeSpan.FromDays(4)))
                                { flag = 1; }                                
                            }
                        }

                        if (flag == 1)
                        { Fresh.Add(1); }
                        else
                        { Fresh.Add(0); }
                        b++;
                    }                    
                    Fresh_arr.Add(Fresh.ToList());
                    a++;
                }
                ViewBag.Fresh_arr = Fresh_arr;
                //ViewBag.Fresh_arr = JsonConvert.SerializeObject(Fresh_arr);
            }

            ViewBag.Days = Days;
            ViewBag.Months = Months;
            if (error_str != "")
            { ViewBag.Error += " Ошибка: " + error_str; }

            ViewBag.Subject = subject;
            ViewBag.UnitID = unit_id;
            ViewBag.Klass = klass;
            ViewBag.School = school;
            ViewBag.Flag = 1;

            return View();
        }

        // GET: /Operator/Hometask
        public ActionResult Hometask(string school)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            ViewBag.SList = Schools;

            ViewBag.UnitID = "";
            ViewBag.Klass = "";
            ViewBag.School = (school == null) ? "" : school;
            ViewBag.Flag = 0;
            ViewBag.Date = "";

            return View();
        }

        // Post: /Operator/Hometask
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Hometask(string school, int unit_id, string klass, DateTime date)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            ViewBag.SList = Schools;

            ViewBag.Date = date;
            ViewBag.UnitID = unit_id;
            ViewBag.Klass = klass;
            ViewBag.School = school;
            ViewBag.Flag = 1;

            ViewBag.HomeTasks = new List<WebServiceReference2.ДЗ_Строка>();

            try
            {
                var HomeTasks = WebApiConfig2.WebClient.ПолучитьДомашниеЗадания(date, unit_id).ToList();
                if (HomeTasks.Count > 0)
                { ViewBag.HomeTasks = HomeTasks; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date: " + date.ToString("dd.MM.yyyy") + " unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьДомашниеЗадания " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            return View();
        }

        // GET: /Operator/Album
        public ActionResult Album(string school)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            ViewBag.SList = Schools;

            ViewBag.UnitID = "";
            ViewBag.Klass = "";
            ViewBag.School = (school == null) ? "" : school;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Album
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Album(string school, int unit_id, string klass)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            ViewBag.SList = Schools;

            ViewBag.UnitID = unit_id;
            ViewBag.Klass = klass;
            ViewBag.School = school;
            ViewBag.Flag = 1;

            ViewBag.Today = DateTime.Today;
            ViewBag.Tomorrow = DateTime.Today.AddDays(1);
            ViewBag.Tomorrow2 = DateTime.Today.AddDays(2);

            var Pupils = new List<WebServiceReference2.Ученик_Строка>();

            try
            {
                Pupils = WebApiConfig2.WebClient.ПолучитьУчеников(unit_id).ToList();
                if (Pupils.Count > 0)
                { ViewBag.Pupils = Pupils; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.Pupils = null;

                var DB = new DataContext();
                var error = new Error();
                string str = " unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьУчеников " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            return View();
        }

        // POST: /Operator/GetUnitsGrid
        [HttpPost]
        public ActionResult GetUnitsGrid(int school)
        {
            UserProfile user = null;
            string[] UserUnits = null;
            int null_flag = 1;

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
                if (user.Units != null && user.Units != "")
                {
                    var separators = new string[] { " ", "," };
                    UserUnits = user.Units.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserUnits.Length > 0)
                    { null_flag = 0; }
                }
            }

            var Units = WebApiConfig2.WebClient.ПолучитьКлассы(school).ToList();
            var Классы = new List<List<WebServiceReference2.Класс_Строка>>();
            for (int i = 1; i <= 11; i++)
            {
                var parallel = new List<WebServiceReference2.Класс_Строка>();
                foreach (var unit in Units)
                {
                    if (null_flag == 1 || UserUnits.Contains(unit.Код.ToString()))
                    {
                        var regex = new Regex(@"^" + i.ToString() + @"\D.+$");
                        if (regex.IsMatch(unit.Наименование))
                        { parallel.Add(unit); }
                    }
                }
                if (parallel.Count > 0)
                { Классы.Add(parallel); }
            }
            ViewBag.Units = Классы;

            return PartialView("GetUnitsGrid");
        }

        // POST: /Operator/GetUnits
        [HttpPost]
        public ActionResult GetUnits(int school, int whole)
        {
            UserProfile user = null;
            string[] UserUnits = null;
            int null_flag = 1;

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
                if (user.Units != null && user.Units != "")
                {
                    var separators = new string[] { " ", "," };
                    UserUnits = user.Units.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserUnits.Length > 0)
                    { null_flag = 0; }
                    whole = 0;
                }
            }

            var Units = WebApiConfig2.WebClient.ПолучитьКлассы(school).ToList();
            var Классы = new List<WebServiceReference2.Класс_Строка>();
            for (int i = 1; i <= 11; i++)
            {
                foreach (var unit in Units)
                {
                    if (null_flag == 1 || UserUnits.Contains(unit.Код.ToString()))
                    {
                        var regex = new Regex(@"^" + i.ToString() + @"\D.+$");
                        if (regex.IsMatch(unit.Наименование))
                        { Классы.Add(unit); }
                    }
                }
            }

            ViewBag.Units = Классы;
            ViewBag.Whole = whole;

            return PartialView("GetUnits");
        }

        // GET: /Operator/Stats_absence_short
        public ActionResult Stats_absence_short(string date1, string date2, string school, string unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 2, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 3, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 4, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;
            ViewBag.Whole = 1;

            return View();
        }

        // Post: /Operator/Stats_absence_short
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_absence_short(string date1, string date2, int school, int unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 2, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 3, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 4, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);
            int days = Date2.Subtract(Date1).Days + 1;
            int study_days = 0;

            for (int i = 0; i < days; i++)
            {
                var day = Date1.AddDays(i).DayOfWeek;
                if (day != DayOfWeek.Saturday && day != DayOfWeek.Sunday)
                { study_days++; }
            }

            var Reports_all = new List<Report_pupil>();

            var Units = WebApiConfig.WebClient.GetUnitsForPupils(school).ToList();

            var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school, unit_id);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school.ToString() + " unit: " + unit_id;
                error.Command = "ReportByVisitsOfPupils " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            if (row.Way == 1)
                            { Reports_all.Add(new Report_pupil { Name = row.EmployeeFIO, Klass = row.AdditionInformation, Date = row.Date.ToString("dd.MM.yyyy"), Count = 1 }); }
                            if (row.Way == -1)
                            { Reports_all.Add(new Report_pupil { Name = row.EmployeeFIO, Klass = row.AdditionInformation, Date = "", Count = 0 }); }

                            var first_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                            var last_val = Reports_all.FindLast(r => r.Name == row.EmployeeFIO);
                            if (first_val != null && last_val != null && first_val != last_val)
                            {
                                Report_pupil new_val;
                                if (first_val.Date != last_val.Date)
                                { new_val = new Report_pupil { Name = first_val.Name, Klass = first_val.Klass, Date = last_val.Date, Count = first_val.Count + last_val.Count }; }
                                else
                                { new_val = new Report_pupil { Name = first_val.Name, Klass = first_val.Klass, Date = last_val.Date, Count = first_val.Count }; }

                                var n = Reports_all.IndexOf(first_val);
                                Reports_all[n] = new_val;
                                n = Reports_all.IndexOf(last_val);
                                Reports_all.RemoveAt(n);
                            }
                        }
                    }
                    
                    for (int i = 0; i < Reports_all.Count; i++)
                    {
                        var new_val = new Report_pupil { Name = Reports_all[i].Name, Klass = Reports_all[i].Klass, Count = study_days - Reports_all[i].Count };
                        Reports_all[i] = new_val;
                    }

                    Reports_all.Sort((x, y) =>
                    {
                        if (x.Klass != y.Klass)
                        {
                            var regex1 = new Regex(@"^\d\D.+$");
                            var regex2 = new Regex(@"^\d\d\D.+$");

                            if (regex1.IsMatch(x.Klass) && regex2.IsMatch(y.Klass))
                            { return -1; }

                            if (regex1.IsMatch(y.Klass) && regex2.IsMatch(x.Klass))
                            { return 1; }

                            return x.Klass.CompareTo(y.Klass);
                        }

                        return x.Name.CompareTo(y.Name);
                    });
                }
            }

            int Total_absence = 0;

            foreach (var item in Reports_all)
            {
                Total_absence += item.Count;
            }

            ViewBag.Total_absence = Total_absence;

            if (Reports_all.Count == 0)
            { ViewBag.Error = "Нет результатов."; }

            ViewBag.Report = Reports_all;
            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;
            ViewBag.Whole = 1;
            
            return View();
        }

        // GET: /Operator/Stats_absence_long
        public ActionResult Stats_absence_long(string date1, string date2, string school, string unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 2, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 3, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 4, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Stats_absence_long
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_absence_long(string date1, string date2, int school, int unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 2, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 3, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 4, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);
            int days = Date2.Subtract(Date1).Days + 1;
            List<DateTime> Days = new List<DateTime>();

            for (int i = 0; i < days; i++)
            {
                var day = Date1.AddDays(i);
                if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                { Days.Add(day); }
            }

            var Reports_all = new List<Report_pupil_1adv>();
            var Units = WebApiConfig.WebClient.GetUnitsForPupils(school).ToList();

            if (Days.Count > 0)
            {
                var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school, unit_id);
                if (report.IsError == true)
                {
                    ViewBag.Error = report.ErrorMessage;
                    ViewBag.Report = null;

                    var DB = new DataContext();
                    var error = new Error();
                    string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school.ToString() + " unit: " + unit_id;
                    error.Command = "ReportByVisitsOfPupils " + str;
                    error.Text = report.ErrorMessage;
                    error.Date = DateTime.Now;
                    DB.Errors.Add(error);
                    DB.SaveChanges();
                }
                else
                {
                    var Reports = report.Result;
                    if (Reports.Count > 0)
                    {
                        foreach (var row in Reports)
                        {
                            if (FIO == "" || FIO == row.EmployeeFIO)
                            {
                                string date = row.Date.ToString("dd.MM.yyyy");

                                if (row.Way == 1)
                                {
                                    Reports_all.Add(new Report_pupil_1adv
                                    {
                                        Name = row.EmployeeFIO,
                                        Klass = row.AdditionInformation,
                                        Date = new List<string> { date },
                                        Present = new List<int> { 1 }
                                    }
                                    );
                                }
                                else if (row.Way == 2)
                                {
                                    Reports_all.Add(new Report_pupil_1adv
                                    {
                                        Name = row.EmployeeFIO,
                                        Klass = row.AdditionInformation,
                                        Date = new List<string> { date },
                                        Present = new List<int> { 1 }
                                    }
                                    );
                                }
                                else if (row.Way == -1)
                                {
                                    Reports_all.Add(new Report_pupil_1adv
                                    {
                                        Name = row.EmployeeFIO,
                                        Klass = row.AdditionInformation,
                                        Date = new List<string> { },
                                        Present = new List<int> { }
                                    }
                                    );
                                }

                                var first_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                var last_val = Reports_all.FindLast(r => r.Name == row.EmployeeFIO);
                                if (first_val != null && last_val != null && first_val != last_val)
                                {
                                    var new_val = new Report_pupil_1adv
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Date = first_val.Date.Concat(last_val.Date).ToList(),
                                        Present = first_val.Present.Concat(last_val.Present).ToList()
                                    };

                                    var n = Reports_all.IndexOf(first_val);
                                    Reports_all[n] = new_val;
                                    n = Reports_all.IndexOf(last_val);
                                    Reports_all.RemoveAt(n);
                                }
                            }
                        }
                        Reports_all.Sort((x, y) => x.Name.CompareTo(y.Name));
                    }
                }
            }
            
            /*
            int Total_absence = 0;

            foreach (var item in Reports_all)
            {
                Total_absence += item.Present.Count;
            }

            ViewBag.Total_absence = Total_absence;
            */

            if (Reports_all.Count == 0)
            { ViewBag.Error = "Нет результатов."; }

            ViewBag.Days = Days;
            ViewBag.Report = Reports_all;
            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;

            return View();
        }

        // GET: /Operator/Stats_time_pupils_row
        public ActionResult Stats_time_pupils_row(string date1, string date2, string school, string unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 2, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;
            ViewBag.Whole = 1;

            return View();
        }

        // Post: /Operator/Stats_time_pupils_row
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_time_pupils_row(string date1, string date2, int school, int unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 2, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);

            var Reports_all = new List<List<Report_pupil_adv>>();
            var Reports_day = new List<Report_pupil_adv>();
            var Units = WebApiConfig.WebClient.GetUnitsForPupils(school).ToList();

            var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school, unit_id);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school.ToString() + " unit: " + unit_id;
                error.Command = "ReportByVisitsOfPupils " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));
                    int i = 0;
                    var Dates = new List<string>();
                    string Date = Reports[0].Date.ToString("dd MMMM yyyy");
                    Dates.Add(Date);

                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string d = row.Date.ToString("dd MMMM yyyy");

                            if (d != Date && d != "01 января 0001")
                            {
                                Dates.Add(d);
                                Date = d;
                                i++;
                                Reports_day.Sort((x, y) =>
                                {
                                    if (x.Klass != y.Klass)
                                    {
                                        var regex1 = new Regex(@"^\d\D.+$");
                                        var regex2 = new Regex(@"^\d\d\D.+$");

                                        if (regex1.IsMatch(x.Klass) && regex2.IsMatch(y.Klass))
                                        { return -1; }

                                        if (regex1.IsMatch(y.Klass) && regex2.IsMatch(x.Klass))
                                        { return 1; }

                                        return x.Klass.CompareTo(y.Klass);
                                    }

                                    return x.Name.CompareTo(y.Name);
                                });

                                Reports_all.Add(Reports_day);
                                Reports_day = new List<Report_pupil_adv>();
                            }
                            string time = row.Date.ToString("HH:mm:ss");
                            var NewTime = DateTime.ParseExact(time, "HH:mm:ss", CultureInfo.InvariantCulture);

                            if (row.Way == 1)
                            {
                                Reports_day.Add(new Report_pupil_adv
                                {
                                    Name = row.EmployeeFIO,
                                    Klass = row.AdditionInformation,
                                    Come = new List<string> { time },
                                    Leave = new List<string> { "--" }
                                }
                                );
                            }
                            else if (row.Way == 2)
                            {
                                Reports_day.Add(new Report_pupil_adv
                                {
                                    Name = row.EmployeeFIO,
                                    Klass = row.AdditionInformation,
                                    Come = new List<string> { "--" },
                                    Leave = new List<string> { time }
                                }
                                );
                            }
                            else if (row.Way == -1)
                            {
                                Reports_day.Add(new Report_pupil_adv
                                {
                                    Name = row.EmployeeFIO,
                                    Klass = row.AdditionInformation,
                                    Come = new List<string> { "--" },
                                    Leave = new List<string> { "--" }
                                }
                                );
                            }

                            var first_val = Reports_day.Find(r => r.Name == row.EmployeeFIO);
                            var last_val = Reports_day.FindLast(r => r.Name == row.EmployeeFIO);
                            if (first_val != null && last_val != null && first_val != last_val)
                            {
                                Report_pupil_adv new_val = null;
                                if (first_val.Leave.Last() == "--" && last_val.Come.First() == "--")
                                {
                                    var leave = first_val.Leave;
                                    leave.RemoveAt(leave.LastIndexOf("--"));

                                    var come = last_val.Come;
                                    come.RemoveAt(come.IndexOf("--"));

                                    new_val = new Report_pupil_adv
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Come = first_val.Come.Concat(come).ToList(),
                                        Leave = leave.Concat(last_val.Leave).ToList()
                                    };
                                }
                                else
                                {
                                    new_val = new Report_pupil_adv
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Come = first_val.Come.Concat(last_val.Come).ToList(),
                                        Leave = first_val.Leave.Concat(last_val.Leave).ToList()
                                    };
                                }

                                var n = Reports_day.IndexOf(first_val);
                                Reports_day[n] = new_val;
                                n = Reports_day.IndexOf(last_val);
                                Reports_day.RemoveAt(n);
                            }
                        }
                    }

                    Reports_day.Sort((x, y) =>
                    {
                        if (x.Klass != y.Klass)
                        {
                            var regex1 = new Regex(@"^\d\D.+$");
                            var regex2 = new Regex(@"^\d\d\D.+$");

                            if (regex1.IsMatch(x.Klass) && regex2.IsMatch(y.Klass))
                            { return -1; }

                            if (regex1.IsMatch(y.Klass) && regex2.IsMatch(x.Klass))
                            { return 1; }

                            return x.Klass.CompareTo(y.Klass);
                        }

                        return x.Name.CompareTo(y.Name);
                    });

                    Reports_all.Add(Reports_day);

                    ViewBag.Dates = Dates;
                }
            }

            ViewBag.Report = Reports_all;

            if (Reports_all.Count == 0)
            { ViewBag.Error = "Нет результатов."; }

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;
            ViewBag.Whole = 1;

            return View();
        }

        // GET: /Operator/Stats_time_pupils_col
        public ActionResult Stats_time_pupils_col(string date1, string date2, string school, string unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 2, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;
            ViewBag.Whole = 1;

            return View();
        }

        // Post: /Operator/Stats_time_pupils_col
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_time_pupils_col(string date1, string date2, int school, int unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 2, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 6, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);
            int days = Date2.Subtract(Date1).Days + 1;
            var Days = new List<string>();

            for (int i = 0; i < days; i++)
            {
                var day = Date1.AddDays(i);
                if (day.DayOfWeek != DayOfWeek.Saturday && day.DayOfWeek != DayOfWeek.Sunday)
                { Days.Add(day.ToString("dd.MM.yyyy")); }
            }

            var Reports_all = new List<Report_pupil_adv_col>();
            var Units = WebApiConfig.WebClient.GetUnitsForPupils(school).ToList();

            var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school, unit_id);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school.ToString() + " unit: " + unit_id;
                error.Command = "ReportByVisitsOfPupils " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));

                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string time = row.Date.ToString("HH:mm:ss");
                            string date = row.Date.ToString("dd.MM.yyyy");
                            var NewTime = DateTime.ParseExact(time, "HH:mm:ss", CultureInfo.InvariantCulture);

                            if (row.Way == 1)
                            {
                                Reports_all.Add(new Report_pupil_adv_col
                                {
                                    Name = row.EmployeeFIO,
                                    Klass = row.AdditionInformation,
                                    Come = new List<string> { time },
                                    Leave = new List<string> { "--" },
                                    Date = new List<string> { date }
                                }
                                );
                            }
                            else if (row.Way == 2)
                            {
                                Reports_all.Add(new Report_pupil_adv_col
                                {
                                    Name = row.EmployeeFIO,
                                    Klass = row.AdditionInformation,
                                    Come = new List<string> { "--" },
                                    Leave = new List<string> { time },
                                    Date = new List<string> { date }
                                }
                                );
                            }
                            else if (row.Way == -1)
                            {
                                Reports_all.Add(new Report_pupil_adv_col
                                {
                                    Name = row.EmployeeFIO,
                                    Klass = row.AdditionInformation,
                                    Come = new List<string> { "--" },
                                    Leave = new List<string> { "--" },
                                    Date = new List<string> { date }
                                }
                                );
                            }

                            var first_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                            var last_val = Reports_all.FindLast(r => r.Name == row.EmployeeFIO);
                            if (first_val != null && last_val != null && first_val != last_val)
                            {
                                Report_pupil_adv_col new_val = null;
                                if (first_val.Leave.Last() == "--" && last_val.Come.First() == "--"
                                    && first_val.Date.Last() == last_val.Date.First())
                                {
                                    var leave = first_val.Leave;
                                    leave.RemoveAt(leave.LastIndexOf("--"));

                                    var come = last_val.Come;
                                    come.RemoveAt(come.IndexOf("--"));

                                    new_val = new Report_pupil_adv_col
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Come = first_val.Come.Concat(come).ToList(),
                                        Leave = leave.Concat(last_val.Leave).ToList(),
                                        Date = first_val.Date
                                    };
                                }
                                else
                                {
                                    new_val = new Report_pupil_adv_col
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Come = first_val.Come.Concat(last_val.Come).ToList(),
                                        Leave = first_val.Leave.Concat(last_val.Leave).ToList(),
                                        Date = first_val.Date.Concat(last_val.Date).ToList()
                                    };
                                }

                                var n = Reports_all.IndexOf(first_val);
                                Reports_all[n] = new_val;
                                n = Reports_all.IndexOf(last_val);
                                Reports_all.RemoveAt(n);
                            }
                        }
                    }

                    Reports_all.Sort((x, y) =>
                    {
                        if (x.Klass != y.Klass)
                        {
                            var regex1 = new Regex(@"^\d\D.+$");
                            var regex2 = new Regex(@"^\d\d\D.+$");

                            if (regex1.IsMatch(x.Klass) && regex2.IsMatch(y.Klass))
                            { return -1; }

                            if (regex1.IsMatch(y.Klass) && regex2.IsMatch(x.Klass))
                            { return 1; }

                            return x.Klass.CompareTo(y.Klass);
                        }

                        return x.Name.CompareTo(y.Name);
                    });

                    ViewBag.Days = Days;
                }
            }
            
            ViewBag.Report = Reports_all;

            if (Reports_all.Count == 0)
            { ViewBag.Error = "Нет результатов."; }

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;
            ViewBag.Whole = 1;

            return View();
        }

        // GET: /Operator/Stats_late_class
        public ActionResult Stats_late_class(string date1, string date2, string school, string unit_id, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 2, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Stats_late_class
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_late_class(string date1, string date2, int school, int unit_id, string FIO)
        {
            string error_str = "";

            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 2, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            var Units = WebApiConfig.WebClient.GetUnitsForPupils(school).ToList();
            var Klasses = new List<WebServiceReference.WSUnit>();
            var TTs = new List<List<WebServiceReference.WsTimetable>>();
            int k = 0;

            if (unit_id == 0)
            {
                for (int i = 1; i <= 11; i++)
                {
                    foreach (var unit in Units)
                    {
                        var regex = new Regex(@"^" + i.ToString() + @"\D.+$");
                        if (regex.IsMatch(unit.Name))
                        { Klasses.Add(unit); }
                    }
                }
                k = Klasses.Count;
            }
            else
            {
                var TimeTable = WebApiConfig.WebClient.GetTimetables(unit_id).ToList();
                if (TimeTable.Count == 0)
                { error_str += "Расписание пустое. "; }
                else
                { TTs.Add(TimeTable); }
            }

            for (int i = 0; i < k; i++)
            {
                var TimeTable = WebApiConfig.WebClient.GetTimetables(Klasses[i].ID).ToList();
                if (TimeTable.Count > 0)
                { TTs.Add(TimeTable); }
            }

            ViewBag.TT = TTs;
            if (TTs.Count == 0 && error_str == "")
            { error_str += "Расписание пустое. "; }

            var Days = new List<List<DateTime>>();
            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);
            int days = Date2.Subtract(Date1).Days;
            var Times = new List<List<TimeSpan>>();
            var ZeroTime = new TimeSpan(0, 0, 0, 0);

            for (int t = 0; t < TTs.Count; t++)
            {
                var Days_t = new List<DateTime>();
                var Times_t = new List<TimeSpan>();

                for (int i = 0; i <= days; i++)
                {
                    var add = TimeSpan.FromDays(i);
                    DateTime day = Date1.Add(add);
                    if (day.DayOfWeek != DayOfWeek.Sunday && day.DayOfWeek != DayOfWeek.Saturday)
                    {
                        Days_t.Add(day);
                        Times_t.Add(ZeroTime);
                    }
                }

                for (int i = Days_t.Count - 1; i >= 0; i--)
                {
                    string day_str = WeekDay(Days_t[i]);
                    foreach (var item in TTs[t])
                    {
                        if (item.Day.Name == day_str)
                        {
                            if (Times_t[i] == ZeroTime || item.NumLesson.StartTime < Times_t[i])
                            { Times_t[i] = item.NumLesson.StartTime; }
                        }
                    }
                }
                Days.Add(Days_t);
                Times.Add(Times_t);
            }

            ViewBag.Days = Days;
            ViewBag.Times = Times;

            var Reports_all = new List<Report_pupil_late>();

            var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school, unit_id);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school.ToString() + " unit: " + unit_id;
                error.Command = "ReportByVisitsOfPupils " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));

                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string date_str = row.Date.ToString("dd.MM.yyyy");
                            var date = DateTime.ParseExact(date_str, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                            int i = -1;
                            if (Days.Count > 0)
                            { i = Days[0].IndexOf(date); }
                            if (i != -1)
                            {
                                var time = date + Times[0][i];

                                if (row.Way == 1)
                                {
                                    var old_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                    int flag = 0;
                                    if (old_val != null)
                                    {
                                        foreach (var item in old_val.Time)
                                        {
                                            if (item.ToString("dd.MM.yyyy") == date_str)
                                            { flag++; }
                                        }
                                    }
                                    if (flag == 0)
                                    {
                                        TimeSpan x = row.Date.Subtract(time);
                                        x = (x.Ticks < 0) ? ZeroTime : x;
                                        int y = (x.Ticks > 1) ? 1 : 0;

                                        Reports_all.Add(new Report_pupil_late
                                        {
                                            Name = row.EmployeeFIO,
                                            Klass = row.AdditionInformation,
                                            Time = new List<DateTime> { row.Date },
                                            Late = new List<string> { x.ToString(@"hh\:mm\:ss") },
                                            Total = x,
                                            Count = y
                                        }
                                        );
                                    }
                                }
                                else if (row.Way == 2)
                                {
                                    var old_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                    int flag = 0;
                                    if (old_val != null)
                                    {
                                        foreach (var item in old_val.Time)
                                        {
                                            if (item.ToString("dd.MM.yyyy") == date_str)
                                            { flag++; }
                                        }
                                    }
                                    if (flag == 0)
                                    {
                                        TimeSpan x = row.Date.Subtract(time);
                                        x = (x.Ticks < 0) ? ZeroTime : x;
                                        int y = (x.Ticks > 1) ? 1 : 0;

                                        Reports_all.Add(new Report_pupil_late
                                        {
                                            Name = row.EmployeeFIO,
                                            Klass = row.AdditionInformation,
                                            Time = new List<DateTime> { row.Date },
                                            Late = new List<string> { x.ToString(@"hh\:mm\:ss") },
                                            Total = x,
                                            Count = y
                                        }
                                        );
                                    }
                                }
                                else if (row.Way == -1)
                                {
                                    Reports_all.Add(new Report_pupil_late
                                    {
                                        Name = row.EmployeeFIO,
                                        Klass = row.AdditionInformation,
                                        Time = new List<DateTime>(),
                                        Late = new List<string>(),
                                        Total = new TimeSpan(1),
                                        Count = 0
                                    }
                                    );
                                }

                                var first_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                var last_val = Reports_all.FindLast(r => r.Name == row.EmployeeFIO);
                                if (first_val != null && last_val != null && first_val != last_val)
                                {
                                    var new_val = new Report_pupil_late
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Time = first_val.Time.Concat(last_val.Time).ToList(),
                                        Late = first_val.Late.Concat(last_val.Late).ToList(),
                                        Total = first_val.Total.Add(last_val.Total),
                                        Count = first_val.Count + last_val.Count
                                    };
                                    var n = Reports_all.IndexOf(first_val);
                                    Reports_all[n] = new_val;
                                    n = Reports_all.IndexOf(last_val);
                                    Reports_all.RemoveAt(n);
                                }
                            }
                        }
                    }
                    
                    var Total_sum = ZeroTime;
                    var Count_sum = 0;

                    for (int i = 0; i < Reports_all.Count; i++)
                    {
                        Total_sum += Reports_all[i].Total;
                        Count_sum += Reports_all[i].Count;

                        for (int j = Reports_all[i].Time.Count - 1; j >= 0; j--)
                        {
                            if (TimeSpan.ParseExact(Reports_all[i].Late[j], @"hh\:mm\:ss", CultureInfo.InvariantCulture).Ticks == 0)
                            {
                                Reports_all[i].Time.RemoveAt(j);
                                Reports_all[i].Late.RemoveAt(j);
                            }
                        }
                    }

                    ViewBag.Total_sum = Total_sum;
                    ViewBag.Count_sum = Count_sum;

                    Reports_all.Sort((x, y) =>
                    {
                        if (x.Klass != y.Klass)
                        {
                            var regex1 = new Regex(@"^\d\D.+$");
                            var regex2 = new Regex(@"^\d\d\D.+$");

                            if (regex1.IsMatch(x.Klass) && regex2.IsMatch(y.Klass))
                            { return -1; }

                            if (regex1.IsMatch(y.Klass) && regex2.IsMatch(x.Klass))
                            { return 1; }

                            return x.Klass.CompareTo(y.Klass);
                        }

                        return x.Name.CompareTo(y.Name);
                    });

                }
            }

            if (Reports_all.Count == 0)
            { ViewBag.Error = "Нет результатов."; }
            if (error_str != "")
            { ViewBag.Error += " Ошибка: " + error_str; }

            ViewBag.Report = Reports_all;
            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.Unit = unit_id;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;

            return View();
        }

        // GET: /Operator/Stats_late_school
        public ActionResult Stats_late_school(string date1, string date2, string school, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 2, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Stats_late_school
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_late_school(string date1, string date2, int school, string FIO)
        {
            string error_str = "";

            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
                new ReportType {ID = 2, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 8, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
            };

            var Units = WebApiConfig.WebClient.GetUnitsForPupils(school).ToList();
            var Klasses = new List<WebServiceReference.WSUnit>();
            var TTs = new List<List<WebServiceReference.WsTimetable>>();

            for (int i = 1; i <= 11; i++)
            {
                foreach (var unit in Units)
                {
                    var regex = new Regex(@"^" + i.ToString() + @"\D.+$");
                    if (regex.IsMatch(unit.Name))
                    { Klasses.Add(unit); }
                }
            }

            for (int i = 0; i < Klasses.Count; i++)
            {
                var TimeTable = WebApiConfig.WebClient.GetTimetables(Klasses[i].ID).ToList();
                if (TimeTable.Count > 0)
                { TTs.Add(TimeTable); }
                else
                { error_str += "Расписание " + Klasses[i].Name + " пустое. "; }
            }

            ViewBag.TT = TTs;
            if (TTs.Count == 0 && error_str == "")
            { error_str += "Расписание пустое. "; }

            var Days = new List<List<DateTime>>();
            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);
            int days = Date2.Subtract(Date1).Days;
            var Times = new List<List<TimeSpan>>();
            var ZeroTime = new TimeSpan(0, 0, 0, 0);

            for (int t = 0; t < TTs.Count; t++)
            {
                var Days_t = new List<DateTime>();
                var Times_t = new List<TimeSpan>();

                for (int i = 0; i <= days; i++)
                {
                    var add = TimeSpan.FromDays(i);
                    DateTime day = Date1.Add(add);
                    if (day.DayOfWeek != DayOfWeek.Sunday && day.DayOfWeek != DayOfWeek.Saturday)
                    {
                        Days_t.Add(day);
                        Times_t.Add(ZeroTime);
                    }
                }

                for (int i = Days_t.Count - 1; i >= 0; i--)
                {
                    string day_str = WeekDay(Days_t[i]);
                    foreach (var item in TTs[t])
                    {
                        if (item.Day.Name == day_str)
                        {
                            if (Times_t[i] == ZeroTime || item.NumLesson.StartTime < Times_t[i])
                            { Times_t[i] = item.NumLesson.StartTime; }
                        }
                    }
                }
                Days.Add(Days_t);
                Times.Add(Times_t);
            }

            ViewBag.Days = Days;
            ViewBag.Times = Times;

            var Reports_all = new List<Report_pupil_late>();

            var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school, 0);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school.ToString() + " unit: 0";
                error.Command = "ReportByVisitsOfPupils " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0 && error_str == "")
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));

                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string klass_str = row.AdditionInformation;
                            int j = 0;

                            for (int m = 0; m < Klasses.Count; m++)
                            {
                                if (Klasses[m].Name == klass_str)
                                { j = m; }
                            }

                            string date_str = row.Date.ToString("dd.MM.yyyy");
                            var date = DateTime.ParseExact(date_str, "dd.MM.yyyy", CultureInfo.InvariantCulture);
                            int i = Days[j].IndexOf(date);
                            if (i != -1)
                            {
                                var time = date + Times[j][i];

                                if (row.Way == 1)
                                {
                                    var old_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                    int flag = 0;
                                    if (old_val != null)
                                    {
                                        foreach (var item in old_val.Time)
                                        {
                                            if (item.ToString("dd.MM.yyyy") == date_str)
                                            { flag++; }
                                        }
                                    }
                                    if (flag == 0)
                                    {
                                        TimeSpan x = row.Date.Subtract(time);
                                        x = (x.Ticks < 0) ? ZeroTime : x;

                                        Reports_all.Add(new Report_pupil_late
                                        {
                                            Name = row.EmployeeFIO,
                                            Klass = row.AdditionInformation,
                                            Time = new List<DateTime> { row.Date },
                                            Late = new List<string> { x.ToString(@"hh\:mm\:ss") },
                                            Total = x,
                                            Count = j
                                        }
                                        );
                                    }
                                }
                                else if (row.Way == 2)
                                {
                                    var old_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                    int flag = 0;
                                    if (old_val != null)
                                    {
                                        foreach (var item in old_val.Time)
                                        {
                                            if (item.ToString("dd.MM.yyyy") == date_str)
                                            { flag++; }
                                        }
                                    }
                                    if (flag == 0)
                                    {
                                        TimeSpan x = row.Date.Subtract(time);
                                        x = (x.Ticks < 0) ? ZeroTime : x;

                                        Reports_all.Add(new Report_pupil_late
                                        {
                                            Name = row.EmployeeFIO,
                                            Klass = row.AdditionInformation,
                                            Time = new List<DateTime> { row.Date },
                                            Late = new List<string> { x.ToString(@"hh\:mm\:ss") },
                                            Total = x,
                                            Count = j
                                        }
                                        );
                                    }
                                }
                                else if (row.Way == -1)
                                {
                                    Reports_all.Add(new Report_pupil_late
                                    {
                                        Name = row.EmployeeFIO,
                                        Klass = row.AdditionInformation,
                                        Time = new List<DateTime>(),
                                        Late = new List<string>(),
                                        Total = new TimeSpan(1),
                                        Count = j
                                    }
                                    );
                                }

                                var first_val = Reports_all.Find(r => r.Name == row.EmployeeFIO);
                                var last_val = Reports_all.FindLast(r => r.Name == row.EmployeeFIO);
                                if (first_val != null && last_val != null && first_val != last_val)
                                {
                                    var new_val = new Report_pupil_late
                                    {
                                        Name = first_val.Name,
                                        Klass = first_val.Klass,
                                        Time = first_val.Time.Concat(last_val.Time).ToList(),
                                        Late = first_val.Late.Concat(last_val.Late).ToList(),
                                        Total = first_val.Total.Add(last_val.Total),
                                        Count = first_val.Count
                                    };
                                    var n = Reports_all.IndexOf(first_val);
                                    Reports_all[n] = new_val;
                                    n = Reports_all.IndexOf(last_val);
                                    Reports_all.RemoveAt(n);
                                }
                            }
                        }
                    }

                    for (int i = 0; i < Reports_all.Count; i++)
                    {
                        for (int j = Reports_all[i].Time.Count - 1; j >= 0; j--)
                        {
                            if (TimeSpan.ParseExact(Reports_all[i].Late[j], @"hh\:mm\:ss", CultureInfo.InvariantCulture).Ticks == 0)
                            {
                                Reports_all[i].Time.RemoveAt(j);
                                Reports_all[i].Late.RemoveAt(j);
                            }
                        }
                    }

                    Reports_all.Sort((x, y) =>
                    {
                        if (x.Klass != y.Klass)
                        {
                            var regex1 = new Regex(@"^\d\D.+$");
                            var regex2 = new Regex(@"^\d\d\D.+$");

                            if (regex1.IsMatch(x.Klass) && regex2.IsMatch(y.Klass))
                            { return -1; }

                            if (regex1.IsMatch(y.Klass) && regex2.IsMatch(x.Klass))
                            { return 1; }

                            return x.Klass.CompareTo(y.Klass);
                        }

                        return x.Name.CompareTo(y.Name);
                    });

                }
            }

            if (Reports_all.Count == 0)
            { ViewBag.Error = "Нет результатов."; }
            if (error_str != "")
            { ViewBag.Error += " Ошибка: " + error_str; }

            ViewBag.Report = Reports_all;
            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;

            return View();
        }

        // GET: /Operator/Stats_stuff_short
        public ActionResult Stats_stuff_short(string date1, string date2, string school, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 2, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 8, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Stats_stuff_short
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_stuff_short(string date1, string date2, int school, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 2, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 8, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
            };

            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);

            var report = WebApiConfig.WebClient.ReportByVisitsOfWorkers(Date1, Date2, school);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + date1 + " date2: " + date2 + " school: " + school.ToString();
                error.Command = "ReportByVisitsOfWorkers " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));
                    int i = 0;
                    var Dates = new List<string>();
                    var Reports_all = new List<List<Report_stuff>>();
                    var Reports_day = new List<Report_stuff>();
                    string Date = Reports[0].Date.ToString("dd MMMM yyyy");
                    Dates.Add(Date);

                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string d = row.Date.ToString("dd MMMM yyyy");
                            string time = row.Date.ToString("HH:mm:ss");
                            var NewTime = DateTime.ParseExact(time, "HH:mm:ss", CultureInfo.InvariantCulture);
                            Report_stuff old_val = null;
                            string post = row.AdditionInformation;

                            if (d != Date && d != "01 января 0001")
                            {
                                Dates.Add(d);
                                Date = d;
                                i++;
                                Reports_day.Sort((x, y) => x.Name.CompareTo(y.Name));
                                for (int r = Reports_day.Count - 1; r >= 0; r--)
                                {
                                    if (Reports_day[r].Post == "IDSmart")
                                    { Reports_day.RemoveAt(r); }
                                }
                                Reports_all.Add(Reports_day);
                                Reports_day = new List<Report_stuff>();
                            }
                            if (row.Way == 1 && post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = time, Leave = "--" }); }
                            else if (row.Way == 2 && post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            {
                                if (Reports_day.Count > 0)
                                { old_val = Reports_day.FindLast(r => r.Name == row.EmployeeFIO); }
                                if (old_val != null)
                                {
                                    var new_val = new Report_stuff { Name = old_val.Name, Post = old_val.Post, Come = old_val.Come, Leave = time };
                                    int n = Reports_day.IndexOf(old_val);

                                    if (old_val.Leave != "--")
                                    {
                                        var OldTime = DateTime.ParseExact(old_val.Leave, "HH:mm:ss", CultureInfo.InvariantCulture);
                                        if (DateTime.Compare(OldTime, NewTime) < 0 && n != -1)
                                        { Reports_day[n] = new_val; }
                                    }
                                    else if (n != -1)
                                    { Reports_day[n] = new_val; }
                                }
                                else
                                { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = "--", Leave = time }); }
                            }
                            else if (post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = "--", Leave = "--" }); }

                            var first_val = Reports_day.Find(r => r.Name == row.EmployeeFIO);
                            var last_val = Reports_day.FindLast(r => r.Name == row.EmployeeFIO);
                            if (first_val != null && last_val != null && first_val != last_val)
                            {
                                if (first_val.Come != "--" || last_val.Leave != "--")
                                {
                                    var new_val = new Report_stuff { Name = first_val.Name, Post = first_val.Post, Come = first_val.Come, Leave = last_val.Leave };
                                    var n = Reports_day.IndexOf(first_val);
                                    Reports_day[n] = new_val;
                                    n = Reports_day.IndexOf(last_val);
                                    Reports_day.RemoveAt(n);
                                }
                            }                            
                        }
                    }
                    Reports_day.Sort((x, y) => x.Name.CompareTo(y.Name));
                    for (int r = Reports_day.Count - 1; r >= 0; r--)
                    {
                        if (Reports_day[r].Post == "IDSmart")
                        { Reports_day.RemoveAt(r); }
                    }
                    Reports_all.Add(Reports_day);

                    ViewBag.Dates = Dates;
                    ViewBag.Report = Reports_all;
                }
                else
                { ViewBag.Error = "Нет результатов."; }
            }

            ViewBag.Date1 = date1;
            ViewBag.Date2 = date2;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;

            return View();
        }

        // GET: /Operator/Stats_stuff_long
        public ActionResult Stats_stuff_long(string date1, string date2, string school, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
                new ReportType {ID = 2, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 8, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
            };

            ViewBag.Date1 = (date1 == null) ? "" : date1;
            ViewBag.Date2 = (date2 == null) ? "" : date2;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 0;

            return View();
        }

        // Post: /Operator/Stats_stuff_long
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Stats_stuff_long(string date1, string date2, int school, string FIO)
        {
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            if (Roles.IsUserInRole("teacher"))
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

                if (user.SchoolID != null && user.SchoolID != "")
                {
                    var separators = new string[] { " ", "," };
                    string[] UserSchools = user.SchoolID.Split(separators, StringSplitOptions.RemoveEmptyEntries);
                    if (UserSchools.Length > 0)
                    {
                        for (int i = Schools.Count - 1; i >= 0; i--)
                        {
                            if (UserSchools.Contains(Schools[i].Код.ToString()) == false)
                            { Schools.RemoveAt(i); }
                        }
                    }
                }
            }

            /*
            for (int i = Schools.Count - 1; i >= 0; i--)
            {
                // Отключить Гимназию №3 от отчета
                if (Schools[i].Код == 8)
                { Schools.RemoveAt(i); }
                // Отключить CШ №4 от отчета
                if (Schools[i].Код == 9)
                { Schools.RemoveAt(i); }
            }
            */
            ViewBag.SList = Schools;

            ViewBag.RType = new List<ReportType>
            {
                new ReportType {ID = 1, Name = "Сотрудники (рабочее время, подробно)", Url = "/Operator/Stats_stuff_long"},
                new ReportType {ID = 2, Name = "Сотрудники (рабочее время, кратко)", Url = "/Operator/Stats_stuff_short"},
                new ReportType {ID = 3, Name = "Учащиеся (пропуски, кратко)", Url = "/Operator/Stats_absence_short"},
                new ReportType {ID = 4, Name = "Учащиеся (пропуски, подробно)", Url = "/Operator/Stats_absence_long"},
                new ReportType {ID = 5, Name = "Учащиеся (учебное время, гориз.)", Url = "/Operator/Stats_time_pupils_row"},
                new ReportType {ID = 6, Name = "Учащиеся (учебное время, верт.)", Url = "/Operator/Stats_time_pupils_col"},
                new ReportType {ID = 7, Name = "Учащиеся (опоздания, класс)", Url = "/Operator/Stats_late_class"},
                new ReportType {ID = 8, Name = "Учащиеся (опоздания, школа)", Url = "/Operator/Stats_late_school"},
            };

            var Date1 = DateTime.ParseExact(date1, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            var Date2 = DateTime.ParseExact(date2, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            Date2 = Date2.AddDays(1).AddSeconds(-1);

            var report = WebApiConfig.WebClient.ReportByVisitsOfWorkers(Date1, Date2, school);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + date1 + " date2: " + date2 + " school: " + school.ToString();
                error.Command = "ReportByVisitsOfWorkers " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));
                    int i = 0;
                    var Dates = new List<string>();
                    string Date = Reports[0].Date.ToString("dd MMMM yyyy");
                    Dates.Add(Date);
                    var Reports_all = new List<List<Report_stuff>>();
                    var Reports_day = new List<Report_stuff>();

                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string d = row.Date.ToString("dd MMMM yyyy");
                            string post = row.AdditionInformation;

                            if (d != Date && d != "01 января 0001")
                            {
                                Dates.Add(d);
                                Date = d;
                                i++;
                                
                                Reports_day.Sort((x, y) =>
                                {
                                    if (x.Name != y.Name)
                                    { return x.Name.CompareTo(y.Name); }

                                    DateTime xd, yd;

                                    try
                                    { xd = DateTime.ParseExact(x.Come, "HH:mm:ss", CultureInfo.InvariantCulture); }
                                    catch (FormatException)
                                    { return -1; }

                                    try
                                    { yd = DateTime.ParseExact(y.Come, "HH:mm:ss", CultureInfo.InvariantCulture); }
                                    catch (FormatException)
                                    { return 1; }

                                    return xd.CompareTo(yd);
                                });

                                for (int r = Reports_day.Count - 1; r >= 0; r--)
                                {
                                    if (Reports_day[r].Post == "IDSmart")
                                    { Reports_day.RemoveAt(r); }
                                }
                                Reports_all.Add(Reports_day);
                                Reports_day = new List<Report_stuff>();
                            }
                            if (row.Way == 1 && post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            {
                                Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = row.Date.ToString("HH:mm:ss"), Leave = "--" });
                            }
                            else if (row.Way == 2 && post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            {
                                Report_stuff old_val = null;
                                if (Reports_day.Count > 0)
                                { old_val = Reports_day.FindLast(r => r.Name == row.EmployeeFIO); }
                                if (old_val != null)
                                {
                                    var new_val = new Report_stuff { Name = old_val.Name, Post = old_val.Post, Come = old_val.Come, Leave = row.Date.ToString("HH:mm:ss") };
                                    int n = Reports_day.IndexOf(old_val);
                                    if (n != -1)
                                    { Reports_day[n] = new_val; }
                                }
                                else
                                { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = "--", Leave = row.Date.ToString("HH:mm:ss") }); }
                            }
                            else if (post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = "--", Leave = "--" }); }
                        }
                    }

                    Reports_day.Sort((x, y) =>
                    {
                        if (x.Name != y.Name)
                        { return x.Name.CompareTo(y.Name); }

                        DateTime xd, yd;

                        try
                        { xd = DateTime.ParseExact(x.Come, "HH:mm:ss", CultureInfo.InvariantCulture); }
                        catch (FormatException)
                        { return -1; }

                        try
                        { yd = DateTime.ParseExact(y.Come, "HH:mm:ss", CultureInfo.InvariantCulture); }
                        catch (FormatException)
                        { return 1; }

                        return xd.CompareTo(yd);
                    });

                    for (int r = Reports_day.Count - 1; r >= 0; r--)
                    {
                        if (Reports_day[r].Post == "IDSmart")
                        { Reports_day.RemoveAt(r); }
                    }
                    Reports_all.Add(Reports_day);

                    ViewBag.Dates = Dates;
                    ViewBag.Report = Reports_all;
                }
                else
                { ViewBag.Error = "Нет результатов."; }
            }

            ViewBag.Date1 = date1;
            ViewBag.Date2 = date2;
            ViewBag.School = school;
            ViewBag.FIO = (FIO == null) ? "" : FIO;
            ViewBag.Flag = 1;

            return View();
        }

        // Post: /Operator/EditHomeTask
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditHomeTask(int name, int pk, string value, DateTime date)
        {
            try
            {
                var res = WebApiConfig2.WebClient.ЗаписатьДомашниеЗадание(name, pk, value, date);
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit: " + name.ToString() + ", subject: " + pk.ToString() + ", task: " + value + ", date: " + date.ToString("dd.MM.yyyy");
                error.Command = "ЗаписатьДомашниеЗадание " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();

                return new HttpStatusCodeResult(400);
            }

            return new HttpStatusCodeResult(200);
        }

        // Post: /Operator/EditJournal
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditJournal(int name, int pk, string value, string date, string comment)
        {
            var regex = new Regex(@"^(10|\d|Н|НУ|НН|НБ|зач|н\/а)((\/|\\)(10|\d))?(\-)?$");
            if (regex.IsMatch(value) == false && value != "")
            { return new HttpStatusCodeResult(415); }

            DateTime Day = DateTime.ParseExact(date, "dd.MM.yyyy", CultureInfo.InvariantCulture);

            try
            {
                var mark = WebApiConfig2.WebClient.ВнестиОценкуУченикаПоПредмету(pk, Day, name, value, "", DateTime.Now);
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "pupil: " + pk.ToString() + ", date: " + date + ", subject: " + name.ToString();
                str += ", mark: " + value + ", comment: " + comment + ", save_date: " + DateTime.Now.ToString("dd.MM.yyyy");
                error.Command = "ВнестиОценкуУченикаПоПредмету " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();

                return new HttpStatusCodeResult(400);
            }

            return new HttpStatusCodeResult(200);
        }

        public string WeekDay(DateTime day)
        {
            switch (day.DayOfWeek.ToString())
            {
                case "Monday": return "Понедельник";
                case "Tuesday": return "Вторник";
                case "Wednesday": return "Среда";
                case "Thursday": return "Четверг";
                case "Friday": return "Пятница";
                case "Saturday": return "Суббота";
            }
            return "Воскресенье";
        }

        public class ReportType
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string Url { get; set; }
        }

        public class Quarter
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string Date1 { get; set; }
            public string Date2 { get; set; }
        }

        public class Month
        {
            public int span { get; set; }
            public string name { get; set; }
        }

        public class Report_stuff
        {
            public string Name { get; set; }
            public string Post { get; set; }
            public string Come { get; set; }
            public string Leave { get; set; }
        }

        public class Report_pupil
        {
            public string Name { get; set; }
            public string Klass { get; set; }
            public string Date { get; set; }
            public int Count { get; set; }
        }

        public class Report_pupil_late
        {
            public string Name { get; set; }
            public string Klass { get; set; }
            public List<DateTime> Time { get; set; }
            public List<string> Late { get; set; }
            public TimeSpan Total { get; set; }
            public int Count { get; set; }
        }

        public class Report_pupil_adv
        {
            public string Name { get; set; }
            public string Klass { get; set; }
            public List<string> Come { get; set; }
            public List<string> Leave { get; set; }
        }

        public class Report_pupil_adv_col
        {
            public string Name { get; set; }
            public string Klass { get; set; }
            public List<string> Come { get; set; }
            public List<string> Leave { get; set; }
            public List<string> Date { get; set; }
        }

        public class Report_pupil_1adv
        {
            public string Name { get; set; }
            public string Klass { get; set; }
            public List<string> Date { get; set; }
            public List<int> Present { get; set; }
        }
    }
}
