﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using WebMatrix.WebData;
using WebSite.Filters;
using WebSite.Helpers;
using WebSite.Models;
using WebSite.WebServiceReference;

namespace WebSite.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.UserName, model.Password, persistCookie: model.RememberMe))
            {
                //var userDB = new UsersContext();
                //var userID = WebSecurity.GetUserId(model.UserName);
                //var rec = userContext.UserProfiles.FirstOrDefault(s => s.UserId == userID);
                //if (rec != null)
                //{
                //var client = WebApiConfig.WebClient.GetProfile(rec.Tel);

                //var dclient = dataContext.Clients.FirstOrDefault(s => s.ID == client.ID);
                //if (dclient == null)
                //{
                //    dclient = new Client();
                //    dataContext.Clients.Add(dclient);
                //}

                //dclient.ID = client.ID;
                //dclient.Name = client.Name;
                //dclient.UserID = userID;

                //dataContext.SaveChanges();

                //foreach (var pupil in client.Pupils)
                //{
                //    var dpupil = dataContext.Pupils.FirstOrDefault(s => s.ID == pupil.ID);
                //    if (dpupil == null)
                //    {
                //dpupil = new Pupil
                //             {
                //                 ID = pupil.ID,
                //                 Name = pupil.Name,
                //                 SchoolID = pupil.School.ID,
                //                 SchoolName = pupil.School.Name,
                //                 UnitID = pupil.Unit.ID,
                //                 UnitName = pupil.Unit.Name,
                //                 UserID = userID
                //             };
                //dataContext.Pupils.Add(dpupil);
                //dataContext.SaveChanges();

                //foreach (var tarif in pupil.Tarifs)
                //{
                //    var dtarif = dataContext.ClientTarifs.FirstOrDefault(s => s.TarifID == tarif.ID);
                //    if (dtarif == null)
                //    {
                //        dtarif = new Tarif();
                //        dataContext.ClientTarifs.Add(dtarif);
                //    }
                //    dtarif.UserID = userID;
                //    dtarif.TarifID = tarif.ID;
                //    dtarif.TarifName = tarif.Name;
                //    dtarif.DateLost = tarif.DateLost;

                //    dataContext.SaveChanges();
                //}
                //}
                //}
                //}

                // статистика входов
                var DB = new DataContext();
                var login_log = new Login_log();
                login_log.username = model.UserName;
                login_log.log_date = DateTime.Now;
                DB.Login_logs.Add(login_log);
                DB.SaveChanges();

                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);

                if (!ControllerHelper.InitializeSession(Session, model.UserName))
                {
                    ModelState.AddModelError("", "Инициализация пользователя не удалась.");
                    return View(model);
                }
                /*
                var user = Session["user"] as WSUserProfile;
                if (user == null)
                    throw new Exception("Пользователь не найден.");

                if (user.Pupil == null)
                    return RedirectToAction("Index", "Client");
                */
                return RedirectToAction("Active_chk", "Account", new { ReturnUrl = returnUrl });
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            ModelState.AddModelError("", "Имя пользователя или пароль указаны неверно.");
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Demo(string parent)
        {
            ViewBag.Texts = GetTexts("/Account/Demo");
            ViewBag.Title = "Демонстрация";

            if (parent != null && parent != "")
            {
                string username = "";
                string password = "";

                if (parent == "1")
                {
                    username = "Тестовый_Родитель";
                    password = "zaqwsxcde";
                }
                if (parent == "2")
                {
                    username = "Тестовый_Учитель";
                    password = "zaqwsxcde";
                }
                if (parent == "3")
                {
                    username = "Тестовый_Администратор";
                    password = "zaqwsxcde";
                }

                if (ModelState.IsValid && WebSecurity.Login(username, password, false))
                {
                    if (ControllerHelper.InitializeSession(Session, username))
                    {
                        return RedirectToAction("Active_chk", "Account");
                    }
                }

                ViewBag.Error = "Ошибка авторизации";
            }

            return View();
        }

        public ActionResult Active_chk(string returnUrl)
        {
            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
            if (user.Active == 1)
            {
                if (Roles.IsUserInRole("managers"))
                {
                    return RedirectToAction("Index", "Manager");
                }
                else if (Roles.IsUserInRole("operator") || Roles.IsUserInRole("teacher"))
                {
                    return RedirectToAction("Index", "Operator");
                }
                else if (Roles.IsUserInRole("principal"))
                {
                    return RedirectToAction("Index", "Principal");
                }
                else if (Roles.IsUserInRole("clients"))
                {
                    return RedirectToAction("Index", "Client");
                }
                else
                {
                    return RedirectToLocal(returnUrl);
                }
            }

            return RedirectToAction("Activate", "Client");
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            Session["user"] = null;
            Session.Clear();

            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            ViewBag.Texts = GetTexts("/Account/Register");
            var myModel = new RegisterModel();
            ViewBag.ParentSelect = new List<Object>
            { 
                new { value = "1" , text = "Родителем"  },
                new { value = "2" , text = "Педагогом" }
            };

            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
            ViewBag.Schools = Schools;
            ViewBag.Date = null;

            return View(myModel);
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterModel model)
        {
            ViewBag.Texts = GetTexts("/Account/Register");
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
            ViewBag.Schools = Schools;
            ViewBag.Date = model.Birthdate.ToString("dd.MM.yyyy");

            ViewBag.ParentSelect = new List<Object>
            { 
                new { value = "1" , text = "Родителем"  },
                new { value = "2" , text = "Педагогом" },
                new { value = "3" , text = "Учеником" }
            };

            //int active = (model.Parent == 2) ? 1 : 0;
            if (model.Parent == 1)
            { model.School_ID = null; }

            TimeSpan ParentAge = new System.TimeSpan(20 * 365, 0, 0, 0);    // Минимальный возраст родителя - 20 лет

            if (ModelState.IsValid)
            {
                if (model.Birthdate < DateTime.Now)
                {
                    // Попытка зарегистрировать пользователя
                    try
                    {
                        //var client = WebApiConfig.WebClient.GetProfile("375" + model.Tel);
                        //if (client == null)
                        //{
                        //    ModelState.AddModelError("Tel", "Пользователь с таким телефоном не найден.");
                        //    return View(model);
                        //}

                        WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new
                        {
                            Phone = "375" + model.Phone,
                            Email = model.Email,
                            Fullname_1 = model.Fullname_1,
                            Fullname_2 = model.Fullname_2,
                            Fullname_3 = model.Fullname_3,
                            Birthdate = model.Birthdate,
                            Parent = model.Parent,
                            SchoolID = model.School_ID,
                            Units = "",
                            Active = 0,
                            Code = 0
                        });
                        Roles.AddUserToRole(model.UserName, "users");
                        WebSecurity.Login(model.UserName, model.Password);
                        if (model.Birthdate < (DateTime.Now - ParentAge) && model.Parent == 1 )
                        {
                            return RedirectToAction("SetChild", "Client");
                        }
                        return RedirectToAction("Activate", "Client");
                    }
                    catch (MembershipCreateUserException e)
                    {
                        ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                    }
                }
            }
            
            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        //
        // POST: /Account/Disassociate

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Disassociate(string provider, string providerUserId)
        {
            string ownerAccount = OAuthWebSecurity.GetUserName(provider, providerUserId);
            ManageMessageId? message = null;

            // Удалять связь учетной записи, только если текущий пользователь — ее владелец
            if (ownerAccount == User.Identity.Name)
            {
                // Транзакция используется, чтобы помешать пользователю удалить учетные данные последнего входа
                using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
                    if (hasLocalAccount || OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name).Count > 1)
                    {
                        OAuthWebSecurity.DeleteAccount(provider, providerUserId);
                        scope.Complete();
                        message = ManageMessageId.RemoveLoginSuccess;
                    }
                }
            }

            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Пароль изменен."
                : message == ManageMessageId.SetPasswordSuccess ? "Пароль задан."
                : message == ManageMessageId.RemoveLoginSuccess ? "Внешняя учетная запись удалена."
                : "";
            ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(LocalPasswordModel model)
        {
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            ViewBag.HasLocalPassword = hasLocalAccount;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasLocalAccount)
            {
                if (ModelState.IsValid)
                {
                    // В ряде случаев при сбое ChangePassword породит исключение, а не вернет false.
                    bool changePasswordSucceeded;
                    try
                    {
                        changePasswordSucceeded = WebSecurity.ChangePassword(User.Identity.Name, model.OldPassword, model.NewPassword);
                    }
                    catch (Exception)
                    {
                        changePasswordSucceeded = false;
                    }

                    if (changePasswordSucceeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        ModelState.AddModelError("", "Неправильный текущий пароль или недопустимый новый пароль.");
                    }
                }
            }
            else
            {
                // У пользователя нет локального пароля, уберите все ошибки проверки, вызванные отсутствующим
                // полем OldPassword
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        WebSecurity.CreateAccount(User.Identity.Name, model.NewPassword);
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    catch (Exception e)
                    {
                        ModelState.AddModelError("", e);
                    }
                }
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            return View(model);
        }

        //
        // POST: /Account/ExternalLogin

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            return new ExternalLoginResult(provider, Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback

        [AllowAnonymous]
        public ActionResult ExternalLoginCallback(string returnUrl)
        {
            AuthenticationResult result = OAuthWebSecurity.VerifyAuthentication(Url.Action("ExternalLoginCallback", new { ReturnUrl = returnUrl }));
            if (!result.IsSuccessful)
            {
                return RedirectToAction("ExternalLoginFailure");
            }

            if (OAuthWebSecurity.Login(result.Provider, result.ProviderUserId, createPersistentCookie: false))
            {
                return RedirectToLocal(returnUrl);
            }

            if (User.Identity.IsAuthenticated)
            {
                // Если текущий пользователь вошел в систему, добавляется новая учетная запись
                OAuthWebSecurity.CreateOrUpdateAccount(result.Provider, result.ProviderUserId, User.Identity.Name);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // Новый пользователь, запрашиваем желаемое имя участника
                string loginData = OAuthWebSecurity.SerializeProviderUserId(result.Provider, result.ProviderUserId);
                ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(result.Provider).DisplayName;
                ViewBag.ReturnUrl = returnUrl;
                return View("ExternalLoginConfirmation", new RegisterExternalLoginModel { UserName = result.UserName, ExternalLoginData = loginData });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLoginConfirmation(RegisterExternalLoginModel model, string returnUrl)
        {
            string provider = null;
            string providerUserId = null;

            if (User.Identity.IsAuthenticated || !OAuthWebSecurity.TryDeserializeProviderUserId(model.ExternalLoginData, out provider, out providerUserId))
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Добавление нового пользователя в базу данных
                using (UsersContext db = new UsersContext())
                {
                    UserProfile user = db.UserProfiles.FirstOrDefault(u => u.UserName.ToLower() == model.UserName.ToLower());
                    // Проверка наличия пользователя в базе данных
                    if (user == null)
                    {
                        // Добавление имени в таблицу профиля
                        db.UserProfiles.Add(new UserProfile { UserName = model.UserName });
                        db.SaveChanges();

                        OAuthWebSecurity.CreateOrUpdateAccount(provider, providerUserId, model.UserName);
                        OAuthWebSecurity.Login(provider, providerUserId, createPersistentCookie: false);

                        return RedirectToLocal(returnUrl);
                    }
                    else
                    {
                        ModelState.AddModelError("UserName", "Имя пользователя уже существует. Введите другое имя пользователя.");
                    }
                }
            }

            ViewBag.ProviderDisplayName = OAuthWebSecurity.GetOAuthClientData(provider).DisplayName;
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // GET: /Account/ExternalLoginFailure

        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [AllowAnonymous]
        [ChildActionOnly]
        public ActionResult ExternalLoginsList(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView("_ExternalLoginsListPartial", OAuthWebSecurity.RegisteredClientData);
        }

        [ChildActionOnly]
        public ActionResult RemoveExternalLogins()
        {
            ICollection<OAuthAccount> accounts = OAuthWebSecurity.GetAccountsFromUserName(User.Identity.Name);
            List<ExternalLogin> externalLogins = new List<ExternalLogin>();
            foreach (OAuthAccount account in accounts)
            {
                AuthenticationClientData clientData = OAuthWebSecurity.GetOAuthClientData(account.Provider);

                externalLogins.Add(new ExternalLogin
                {
                    Provider = account.Provider,
                    ProviderDisplayName = clientData.DisplayName,
                    ProviderUserId = account.ProviderUserId,
                });
            }

            ViewBag.ShowRemoveButton = externalLogins.Count > 1 || OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(User.Identity.Name));
            return PartialView("_RemoveExternalLoginsPartial", externalLogins);
        }
        
        private List<Text> GetTexts(string page)
        {
            var DB = new DataContext();
            IEnumerable<Text> texts = DB.Texts.Where(t => t.Page == page).Select(s => s);

            List<Text> list = new List<Text>();
            foreach (var text in texts)
            {
                if (text.Value != null)
                { list.Add(text); }
            }
            return list;
        }
        #region Вспомогательные методы
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; private set; }
            public string ReturnUrl { get; private set; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // Полный список кодов состояния см. по адресу http://go.microsoft.com/fwlink/?LinkID=177550
            //.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "Имя пользователя уже существует. Введите другое имя пользователя.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "Имя пользователя для данного адреса электронной почты уже существует. Введите другой адрес электронной почты.";

                case MembershipCreateStatus.InvalidPassword:
                    return "Указан недопустимый пароль. Введите допустимое значение пароля.";

                case MembershipCreateStatus.InvalidEmail:
                    return "Указан недопустимый адрес электронной почты. Проверьте значение и повторите попытку.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "Указан недопустимый ответ на вопрос для восстановления пароля. Проверьте значение и повторите попытку.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "Указан недопустимый вопрос для восстановления пароля. Проверьте значение и повторите попытку.";

                case MembershipCreateStatus.InvalidUserName:
                    return "Указано недопустимое имя пользователя. Проверьте значение и повторите попытку.";

                case MembershipCreateStatus.ProviderError:
                    return "Поставщик проверки подлинности вернул ошибку. Проверьте введенное значение и повторите попытку. Если проблему устранить не удастся, обратитесь к системному администратору.";

                case MembershipCreateStatus.UserRejected:
                    return "Запрос создания пользователя был отменен. Проверьте введенное значение и повторите попытку. Если проблему устранить не удастся, обратитесь к системному администратору.";

                default:
                    return "Произошла неизвестная ошибка. Проверьте введенное значение и повторите попытку. Если проблему устранить не удастся, обратитесь к системному администратору.";
            }
        }
        #endregion
    }
}
