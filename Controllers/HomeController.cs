﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;
using WebSite.Models;
using SendingEmailwithoutAttachment.Models;

namespace WebSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Texts = GetTexts("/Home/Index");

            return View();
        }

        public ActionResult Demo(string page)
        {
            ViewBag.Page = page;

            return View();
        }

        /*
        public ActionResult Cart()
        {
            var goods = new List<GoodsModel>
            {
                new GoodsModel { ID = 0, Name = "", Price = "0" },
                new GoodsModel { ID = 1, Name = "СМС Начальный", Price = "20000" },
                new GoodsModel { ID = 2, Name = "СМС Базовый", Price = "30000" },
                new GoodsModel { ID = 3, Name = "Оценки Начальный", Price = "35000" },
                new GoodsModel { ID = 4, Name = "Оценки Базовый", Price = "40000" },
                new GoodsModel { ID = 5, Name = "Д/З Начальный", Price = "15000" },
                new GoodsModel { ID = 6, Name = "Д/З Базовый", Price = "20000" },
                new GoodsModel { ID = 7, Name = "Новая карта доступа", Price = "50000" },
                new GoodsModel { ID = 8, Name = "Восстановление карты", Price = "30000" },
                new GoodsModel { ID = 9, Name = "Новая наклейка", Price = "10000" }
            };
            ViewBag.GoodsList = goods;

            return View();
        }

        //[Authorize]
        //[RequireHttps]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PaySubmit(string phone, string email, int[] goods_id, int[] months)
        {
            ViewBag.test = "1";
            ViewBag.email = email;
            ViewBag.phone = phone;

            var goods = new List<GoodsModel>();
            int total = 0;

            int i =0;
            string str_goods = "";
            foreach (int id in goods_id)
            {
                switch (id)
                {
                    case 0: break;
                    case 1:
                        goods.Add(new GoodsModel { ID = id, Name = "СМС Начальный", Price = "20000", Months = months[i] });
                        str_goods += "СМС Начальный, 20000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 20000 * months[i]; break;
                    case 2:
                        goods.Add(new GoodsModel { ID = id, Name = "СМС Базовый", Price = "30000", Months = months[i] });
                        str_goods += "СМС Базовый, 30000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 30000 * months[i]; break;
                    case 3:
                        goods.Add(new GoodsModel { ID = id, Name = "Оценки Начальный", Price = "35000", Months = months[i] });
                        str_goods += "Оценки Начальный, 35000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 35000 * months[i]; break;
                    case 4:
                        goods.Add(new GoodsModel { ID = id, Name = "Оценки Базовый", Price = "40000", Months = months[i] });
                        str_goods += "Оценки Базовый, 40000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 40000 * months[i]; break;
                    case 5:
                        goods.Add(new GoodsModel { ID = id, Name = "Д/З Начальный", Price = "15000", Months = months[i] });
                        str_goods += "Д/З Начальный, 15000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 15000 * months[i]; break;
                    case 6:
                        goods.Add(new GoodsModel { ID = id, Name = "Д/З Базовый", Price = "20000", Months = months[i] });
                        str_goods += "Д/З Базовый, 20000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 20000 * months[i]; break;
                    case 7:
                        goods.Add(new GoodsModel { ID = id, Name = "Новая карта доступа", Price = "50000", Months = 1 });
                        str_goods += "Новая карта доступа, 50000 руб.;   ";
                        total += 50000; break;
                    case 8:
                        goods.Add(new GoodsModel { ID = id, Name = "Восстановление карты", Price = "30000", Months = 1 });
                        str_goods += "Восстановление карты, 30000 руб.;   ";
                        total += 30000; break;
                    case 9:
                        goods.Add(new GoodsModel { ID = id, Name = "Новая наклейка", Price = "10000", Months = 1 });
                        str_goods += "Новая наклейка, 10000 руб.;   ";
                        total += 10000; break;
                }
                i++;
            }

            ViewBag.SelectedGoodsList = goods;
            ViewBag.total_price = total;

            
            var DB = new DataContext();
            var order = new Order();
            order.phone = phone;
            order.email = email;
            order.date = DateTime.Now;
            order.goods = str_goods;
            order.UserId = 4;
            DB.Orders.Add(order);
            DB.SaveChanges();

            ViewBag.Order_id = order.ID ;
            ViewBag.seed = order.date.ToString("ssmmHHddMMyy");
            
            // подсчет контрольной суммы
            string str = ViewBag.seed + "249227470" + order.ID + ViewBag.test + "BYR" + total + "2cVkxpYfcp";
            byte[] bytes = Encoding.ASCII.GetBytes(str);
            byte[] hashBytes = SHA1.Create().ComputeHash(bytes);
            var sb = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            ViewBag.signature = sb.ToString();

            //return RedirectPermanent("https://secure.sandbox.webpay.by:8843");
            return View();
        }
        */

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendMessage(string name, string email, string phone, string msg)
        {
            string path = HttpContext.Server.MapPath("\\") + "BlackList.txt";
            string BlackList = System.IO.File.ReadAllText(path);

            if (BlackList.IndexOf(email) == -1 && msg.IndexOf("runetki") == -1 && msg.IndexOf("anacron") == -1)
            {
                var DB = new DataContext();
                DB.Messages.Add(new Message
                {
                    Name = name,
                    Email = email,
                    Body = msg,
                    Phone = phone,
                    Date = DateTime.Now
                });
                DB.SaveChanges();

                string Subject = "Отзыв клиента на сайте idsmart.by";
                string Body = "Добрый день. <br> Это автоматическое сообщение было отправлено потому, что на сайте idsmart.by был оставлен следующий отзыв: <br>";
                Body += "Имя: " + name + "<br> Email: " + email + "<br> Телефон: " + phone;
                Body += "<br> Сообщение: <br>" + msg;
                //Body = Body.Replace("\n", Environment.NewLine);

                EMail mail = new EMail();
                mail.SendMail("Email", "idsmart2011@ya.ru", new String[] { Subject, Body });
                //mail.SendMail("Email", "alihsey@bk.ru", new String[] { Subject, Body });
            }
            
            return RedirectToAction("Index");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Страница описания приложения.";
            
            return View();
        }

        public ActionResult News()
        {
            ViewBag.Message = "Новости.";
            

            var DB = new DataContext();
            ViewBag.News = DB.News.Where(a => a.Active == 1).Select(s => s).OrderByDescending(o => o.Date);                        

            return View();
        }

        public ActionResult Success()
        {
            ViewBag.Message = "Оплата успешно завершена";

            return View();
        }

        public ActionResult Cancel()
        {
            ViewBag.Message = "Ошибка при оплате";

            return View();
        }

        [HttpPost]
        public ActionResult Notify(string batch_timestamp, string currency_id, string amount,
            string payment_method, string order_id, int site_order_id, string transaction_id,
            string payment_type, string rrn, string wsb_signature)
        {
            // подсчет контрольной суммы
            string str = batch_timestamp + currency_id + amount + payment_method + order_id +
                site_order_id.ToString() + transaction_id + payment_type + rrn + "2cVkxpYfcp";
            byte[] bytes = Encoding.ASCII.GetBytes(str);
            byte[] hashBytes = MD5.Create().ComputeHash(bytes);
            var sb = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            var signature = sb.ToString();

            if (wsb_signature == signature)
            {
                // запись в базу данных
                var DB = new DataContext();
                var order = DB.Orders.Find(site_order_id);
                order.timestamp = batch_timestamp;
                order.currency_id = currency_id;
                order.amount = amount;
                order.method = payment_method;
                order.order_id = order_id;
                order.transaction_id = transaction_id;
                order.payment_type = payment_type;
                order.rrn = rrn;
                DB.SaveChanges();

                if (payment_type == "1" || payment_type == "4")
                {
                    var userDB = new UsersContext();
                    var user = userDB.UserProfiles.Find(order.UserId);
                    // отправка уведомления
                    string Subject = "Уведомление об оплате заказа №" + site_order_id.ToString();
                    string Body = "Добрый день. <br> Это автоматическое оповещение об успешной оплате заказа №" + site_order_id.ToString();
                    Body += " на услуги сайта IDSmart.by";
                    Body += "<br> ФИО: " + user.fullname;
                    Body += "<br> Телефон: +375" + order.phone + "<br> Email: " + order.email;
                    Body += "<br> Заказ на сумму: " + amount;
                    Body += "<br> Выбранные услуги: " + order.goods;
                    //Body += "<br> Параметры платежа: " + str;

                    EMail mail = new EMail();
                    mail.SendMail("Email", "idsmart2011@ya.ru", new String[] { Subject, Body });
                    //mail.SendMail("Email", "alihsey@bk.ru", new String[] { Subject, Body });
                }
            }

            return new HttpStatusCodeResult(200);
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Страница контактов.";

            return View();
        }

        public ActionResult Scheme()
        {
            var list = GetListScheme();

            for (int i = 52; i < 101; i++)
                list.Add(new object[] { i, "", "" });

            ViewBag.List = list;

            return View();
        }

        public ActionResult Parent()
        {
            ViewBag.Texts = GetTexts("/Home/Parent");

            return View();
        }

        public ActionResult Schools()
        {
            ViewBag.Texts = GetTexts("/Home/Schools");

            return View();
        }

        public ActionResult Services()
        {
            ViewBag.Texts = GetTexts("/Home/Services");

            return View();
        }

        public ActionResult Pays()
        {
            ViewBag.Texts = GetTexts("/Home/Pays");

            var list = GetListScheme();

            for (int i = 52; i < 101; i++)
                list.Add(new object[] { i, "", "" });

            ViewBag.List = list;

            return View();
        }

        public ActionResult Calculate()
        {
            ViewBag.GList = WebApiConfig2.WebClient.ПолучитьШколы().ToList();

            var operList = new List<BaseModel>
                               {
                                   new BaseModel {ID = "0", Name = ""},
                                   new BaseModel {ID = "1", Name = "СМС Начальный"},
                                   new BaseModel {ID = "2", Name = "СМС Базовый"},
                                   //new BaseModel {ID = "3", Name = "Д/З Начальный"},
                                   new BaseModel {ID = "4", Name = "Д/З Базовый"},
                                   //new BaseModel {ID = "5", Name = "Оценки Начальный"},
                                   new BaseModel {ID = "6", Name = "Оценки Базовый"},
                                   new BaseModel {ID = "7", Name = "Новая карта доступа"},
                                   new BaseModel {ID = "8", Name = "Дисконтная карта доступа"},
                                   new BaseModel {ID = "9", Name = "Восстановление карты доступа"},
                                   new BaseModel {ID = "10", Name = "Аватарная карта доступа"},
                                   new BaseModel {ID = "11", Name = "Новая наклейка"}
                                   //new BaseModel {ID = "12", Name = "Услуги дизайнера"}
                               };
            ViewBag.OperList = operList;

            return View();
        }

        [HttpPost]
        public ActionResult Calculate(int[] schoolSelect, string textClass, string textPupil,
             string phone, string[] operSelect, int[] months, string textPay)
        {
            var list = GetEripCode();

            var pupilArray = textPupil.Split(' ');
            if (pupilArray.Length < 2) throw new Exception("Array error.");

            var lPupil = pupilArray[0];
            var fPupil = pupilArray[1];

            if (lPupil.Length < 3 || fPupil.Length < 3) throw new Exception("Error");

            var fio = "";

            for (int i = 0; i < 3; i++)
            {
                var pos = list.IndexOf(lPupil.ToUpper()[i]);
                fio += list.Substring(pos + 2, 2);
            }

            for (int i = 0; i < 3; i++)
            {
                var pos = list.IndexOf(fPupil.ToUpper()[i]);
                fio += list.Substring(pos + 2, 2);
            }

            var pay = "";
            //var bpay = "";

            switch (schoolSelect[0])
            {
                case 1: // Гимназия №8
                    pay = "Г8+";
                    //bpay = "00804";
                    break;
                case 2: // Гимназия №1
                    pay = "Г1+";
                    //bpay = "00104";
                    break;
                case 3: // Гимназия №7
                    pay = "Г7+";
                    break;
                case 4: // СШ №12
                    pay = "Ш12+";
                    break;
                case 5: // СШ №44
                    pay = "Ш44+";
                    break;
                case 6: // Гимназия №5
                    pay = "Г5+";
                    break;
                case 7: // СШ №6
                    pay = "Ш6+";
                    break;
                case 8: // Гимназия №3
                    pay = "Г3+";
                    break;
                case 9: // СШ №4
                    pay = "Ш4+";
                    break;
                case 11: // Гимназия №2
                    pay = "Г2+";
                    break;
                case 12: // СШ №45
                    pay = "Ш45+";
                    break;
                case 13: // СШ №33
                    pay = "Ш33+";
                    break;
                case 14: // СШ №31
                    pay = "Ш31+";
                    break;
                case 15: // СШ №46
                    pay = "Ш46+";
                    break;
                case 10: // Гимназия №1 Солигорск
                    pay = "Г1С+";
                    break;
            }

            pay += (textClass.Length == 2) ? textClass.Substring(0, 2) : textClass.Substring(0, 3);

            //bpay += (textClass.Length == 2) ? "0" + textClass.Substring(0, 1) : textClass.Substring(0, 2);
            //var p = list.IndexOf(textClass.ToUpper().Substring(textClass.Length - 1), 0, StringComparison.Ordinal);
            //bpay += list.Substring(p + 2, 2);

            decimal Sum = 0;

            int j = 0;
            foreach (string oper in operSelect)
            {
                switch (operSelect[j])
                {
                    case "": break;
                    case "СМС Начальный":
                        pay += "+";
                        pay += months[j].ToString();
                        Sum += months[j] * 2m;
                        pay += "22";
                        break;
                    case "СМС Базовый":
                        pay += "+";
                        pay += months[j].ToString();
                        Sum += months[j] * 3m;
                        pay += "21";
                        break;
                    case "Д/З Базовый":
                        pay += "+";
                        pay += months[j].ToString();
                        Sum += months[j] * 2.50m;
                        pay += "31";
                        break;
                    case "Оценки Базовый":
                        pay += "+";
                        pay += months[j].ToString();
                        Sum += months[j] * 5m;
                        pay += "41";
                        break;
                    case "Новая карта доступа":
                        Sum += 7m;
                        pay += "+050";
                        break;
                    case "Дисконтная карта доступа":
                        Sum += 3.50m;
                        pay += "+057";
                        break;
                    case "Восстановление карты доступа":
                        Sum += 3.50m;
                        pay += "+058";
                        break;
                    case "Аватарная карта доступа":
                        Sum += 25m;
                        pay += "+100";
                        break;
                    case "Новая наклейка":
                        Sum += 1.50m;
                        pay += "+070";
                        break;
                    /*
                    case "Д/З Начальный":
                        pay += months[j].ToString();
                        bpay += months[j].ToString();
                        Sum += months[j] * 15000;
                        pay += "23";
                        bpay += "23";
                        break;
                    case "Оценки Начальный":
                        pay += months[j].ToString();
                        bpay += months[j].ToString();
                        Sum += months[j] * 35000;
                        pay += "24";
                        bpay += "24";
                        break;
                    case "Услуги дизайнера":
                        Sum += 25000;
                        pay += "170";
                        bpay += "170";
                        break;
                     */
                }
                j++;
            }

            var textSum = Sum.ToString();                     


            //var res = "<h3 class=\"block-title sub-title\">Для оплаты через: банк, интернет-банк, инфокиоск</h3><table class=\"table table-bordered\"><tbody><tr><td>ФИО</td><td>{0}</td></tr><tr><td>Назначение платежа</td><td>{1}</td></tr><tr><td>Сумма</td><td>{2}</td></tr></tbody></table><h3 class=\"block-title sub-title\">Для оплаты через банкомат</h3><table class=\"table table-bordered\"><tbody><tr><td>ФИО</td><td>{3}</td></tr><tr><td>Назначение платежа</td><td>{4}</td></tr><tr><td>Сумма</td><td>{5}</td></tr></tbody></table>";
            //res = res.Replace("{0}", textPupil);
            //res = res.Replace("{1}", pay);
            //res = res.Replace("{2}", textSum);
            //res = res.Replace("{3}", fio);
            //res = res.Replace("{4}", bpay);
            //res = res.Replace("{5}", textSum);
            //ViewBag.BPay = bpay;

            ViewBag.Pupil = textPupil;
            ViewBag.Phone = "+375"+phone;
            ViewBag.Pay = pay;
            ViewBag.Sum = textSum;
            ViewBag.Fio = fio;

            //return res;
            return PartialView("_Calculate");
        }

        private string GetEripCode()
        {
            return
                "А=01Б=02В=03Г=04Д=05Е=06Ё=07Ж=08З=09И=10Й=11К=12Л=13М=14Н=15О=16П=17Р=18С=19Т=20У=21Ф=22Х=23Ц=24Ч=25Ш=26Щ=27Ь=28Ы=29Ъ=30Э=31Ю=32Я=33";
        }

        private List<object> GetListScheme()
        {
            var list = new List<object>();
            list.Add(new object[] { 1, "001", "А=01" });
            list.Add(new object[] { 2, "002", "Б=02" });
            list.Add(new object[] { 3, "003", "В=03" });
            list.Add(new object[] { 4, "004", "Г=04" });
            list.Add(new object[] { 5, "005", "Д=05" });
            list.Add(new object[] { 6, "006", "Е=06" });
            list.Add(new object[] { 7, "007", "Ё=07" });
            list.Add(new object[] { 8, "008", "Ж=08" });
            list.Add(new object[] { 9, "009", "З=09" });
            list.Add(new object[] { 10, "010", "И=10" });
            list.Add(new object[] { 11, "011", "Й=11" });
            list.Add(new object[] { 12, "012", "К=12" });
            list.Add(new object[] { 13, "013", "Л=13" });
            list.Add(new object[] { 14, "014", "М=14" });
            list.Add(new object[] { 15, "015", "Н=15" });
            list.Add(new object[] { 16, "016", "О=16" });
            list.Add(new object[] { 17, "017", "П=17" });
            list.Add(new object[] { 18, "018", "Р=18" });
            list.Add(new object[] { 19, "019", "С=19" });
            list.Add(new object[] { 20, "020", "Т=20" });
            list.Add(new object[] { 21, "021", "У=21" });
            list.Add(new object[] { 22, "022", "Ф=22" });
            list.Add(new object[] { 23, "023", "Х=23" });
            list.Add(new object[] { 24, "024", "Ц=24" });
            list.Add(new object[] { 25, "025", "Ч=25" });
            list.Add(new object[] { 26, "026", "Ш=26" });
            list.Add(new object[] { 27, "027", "Щ=27" });
            list.Add(new object[] { 28, "028", "Ь=28" });
            list.Add(new object[] { 29, "029", "Ы=29" });
            list.Add(new object[] { 30, "030", "Ъ=30" });
            list.Add(new object[] { 31, "031", "Э=31" });
            list.Add(new object[] { 32, "032", "Ю=32" });
            list.Add(new object[] { 33, "033", "Я=33" });
            list.Add(new object[] { 34, "034", "" });
            list.Add(new object[] { 35, "035", "" });
            list.Add(new object[] { 36, "036", "" });
            list.Add(new object[] { 37, "037", "" });
            list.Add(new object[] { 38, "038", "" });
            list.Add(new object[] { 39, "039", "" });
            list.Add(new object[] { 40, "040", "" });
            list.Add(new object[] { 41, "041", "" });
            list.Add(new object[] { 42, "042", "" });
            list.Add(new object[] { 43, "043", "" });
            list.Add(new object[] { 44, "044", "" });
            list.Add(new object[] { 45, "045", "" });
            list.Add(new object[] { 46, "046", "" });
            list.Add(new object[] { 47, "047", "" });
            list.Add(new object[] { 48, "048", "" });
            list.Add(new object[] { 49, "049", "" });
            list.Add(new object[] { 50, "050", "" });
            list.Add(new object[] { 51, "", "" });

            return list;
        }

        public ActionResult Contract()
        {
            return View();
        }

        public ActionResult ListOfItems()
        {
            return View();
        }

        private List<Text> GetTexts(string page)
        {
            var DB = new DataContext();
            IEnumerable<Text> texts = DB.Texts.Where(t => t.Page == page).Select(s => s);
            
            List<Text> list = new List<Text>();
            foreach (var text in texts)
            {
                if (text.Value != null)
                {   list.Add(text);  }                
            }
            return list;
        }
    }
}
