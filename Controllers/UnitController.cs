﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebSite.Filters;
using WebSite.WebServiceReference;

namespace WebSite.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class UnitController : Controller
    {
        public ActionResult Index(int unitID)
        {
            var user = Session["user"] as WSUserProfile;
            WSUnit unit = null;

            if (user != null && user.Client != null)
            {
                var pupil = user.Client.Pupils.FirstOrDefault(s => s.Unit.ID == unitID);
                if (pupil != null)
                    unit = pupil.Unit;
            }
            else
            {
                if (user != null) unit = user.Pupil.Unit;
            }

            return View(unit);
        }
       
        public ActionResult Pupils(int id)
        {
            var pupils = WebApiConfig.WebClient.GetPupils(id);
            return PartialView(pupils);
        }

        public ActionResult Timetable(int id)
        {
            var list = WebApiConfig.WebClient.GetTimetables(id);
            return PartialView(list.GroupBy(s => s.Day.Name));
        }

        public ActionResult Journals(int id)
        {
            var list = WebApiConfig.WebClient.GetJournals(id);
            return PartialView(list);
        }

        public ActionResult Dnevnik(int id)
        {
            var list = WebApiConfig.WebClient.GetDnevnik(id, DateTime.Parse("20/03/2013"));
            return PartialView(list.GroupBy(s => s.Day.Name));
        }
       
    }
}
