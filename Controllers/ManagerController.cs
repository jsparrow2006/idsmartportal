﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using WebSite.Filters;
using WebSite.Models;

namespace WebSite.Controllers
{
    [Authorize(Roles = "admins, managers")]
    [InitializeSimpleMembership]
    public class ManagerController : Controller
    {
        // GET: /Manager/
        public ActionResult Index()
        {
            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);

            if (user == null)
                return PartialView("Error");

            ViewBag.User = user.fullname;

            return View();
        }

        // Post: /Manager/Edit
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(string name, string pk, string value)
        {
            var DB = new DataContext();
            Text texts = DB.Texts.Where(p => p.Page == name).FirstOrDefault(e => e.Element == pk);
            texts.Value = value;
            DB.SaveChanges();

            return new HttpStatusCodeResult(200);
        }

        public ActionResult News_Add()
        {
            ViewBag.Message = "Добавить новость";

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult News_Add(string title, string body, DateTime date)
        {
            var DB = new DataContext();
            News news = new News();
            news.Date = date;
            news.Title = title;
            news.Body = body;
            news.Active = 1;
            DB.News.Add(news);
            DB.SaveChanges();

            return RedirectPermanent("/Home/News");
        }

        public ActionResult News_Edit(int id)
        {
            ViewBag.Message = "Редактировать новость";

            var DB = new DataContext();
            ViewBag.News = DB.News.Find(id);            

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult News_Edit(int id, string title, string body, DateTime date)
        {
            var DB = new DataContext();
            News news = DB.News.Find(id);
            news.Date = date; 
            news.Title = title;
            news.Body = body;
            DB.SaveChanges();

            return RedirectPermanent("/Home/News");
        }

        public ActionResult News_Hide(int id)
        {
            ViewBag.Message = "Удалить новость";

            var DB = new DataContext();
            ViewBag.News = DB.News.Find(id);  

            return View();
        }

        [HttpPost]
        public ActionResult News_Delete(int id)
        {
            var DB = new DataContext();
            News news = DB.News.Find(id); 
            news.Active = 0;
            DB.SaveChanges();

            return RedirectPermanent("/Home/News");
        }


        public ActionResult User_Add(string success)
        {
            ViewBag.Message = "Добавить клиента";
            //DateTime default_birthdate = DateTime.ParseExact("01.01.1990", "dd.MM.yyyy", CultureInfo.InvariantCulture);
            //ViewBag.Birthdate = default_birthdate;

            if (success != null)
            {
                if (success == "1")
                { ViewBag.Success = "Клиент добавлен успешно"; }
                else
                { ViewBag.Success = "Ошиба добавления клиента: " + success; }
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult User_Add(string username, string password, string client_id,
            string fullname1, string fullname2, string fullname3, string phone, string email, DateTime birthdate)
        {
            ViewBag.Message = "Добавить клиента";
            /*
            DateTime default_birthdate = DateTime.ParseExact("01.01.1990", "dd.MM.yyyy", CultureInfo.InvariantCulture);
            ViewBag.Birthdate = default_birthdate;
            
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
            ViewBag.Schools = Schools;
            ViewBag.Date = model.Birthdate.ToString("dd.MM.yyyy");
            
            ViewBag.ParentSelect = new List<Object>
            { 
                new { value = "1" , text = "Родителем"  },
                new { value = "2" , text = "Педагогом" }
            };

            int parent = 1;
            TimeSpan ParentAge = new System.TimeSpan(20 * 365, 0, 0, 0);    // Минимальный возраст родителя - 20 лет
            if (birthdate < (DateTime.Now - ParentAge) && parent == 1 )
            {}
            */
            var regex1 = new Regex(@"^[а-яёА-ЯЁa-zA-Z0-9\-_]+$");
            var regex2 = new Regex(@"^[0-9]+$");
            var regex3 = new Regex(@"^[а-яёА-ЯЁ\-]+$");

            if (regex1.IsMatch(username) == true && username.Length > 2)
            {
                if (regex2.IsMatch(phone) == true && phone.Length == 9)
                {
                    if (regex3.IsMatch(fullname1) == true && fullname1.Length > 2)
                    {
                        if (regex3.IsMatch(fullname2) == true && fullname2.Length > 2)
                        {
                            if (regex3.IsMatch(fullname3) == true && fullname3.Length > 2)
                            {
                                if (password.Length > 4)
                                {
                                    if (birthdate < DateTime.Now)
                                    {
                                        // Попытка зарегистрировать пользователя
                                        try
                                        {
                                            WebSecurity.CreateUserAndAccount(username, password, new
                                            {
                                                Phone = "375" + phone,
                                                Email = email,
                                                Fullname_1 = fullname1,
                                                Fullname_2 = fullname2,
                                                Fullname_3 = fullname3,
                                                Birthdate = birthdate,
                                                Parent = 1,
                                                ClientID = client_id,
                                                Units = "",
                                                Active = 1,
                                                Code = 0
                                            });
                                            Roles.AddUserToRole(username, "users");
                                            Roles.AddUserToRole(username, "clients");

                                            return RedirectPermanent("/Manager/User_Add?success=1");
                                        }
                                        catch (MembershipCreateUserException e)
                                        {
                                            return RedirectPermanent("/Manager/User_Add?success=" + e.StatusCode.ToString());
                                        }
                                    }
                                    else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_birtdate"); }
                                }
                                else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_password"); }
                            }
                            else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_fullname1"); }
                        }
                        else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_fullname2"); }
                    }
                    else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_fullname3"); }
                }
                else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_phone"); }
            }
            else { return RedirectPermanent("/Manager/User_Add?success=" + "bad_username"); }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            //return View();
        }

        public ActionResult Login_stats()
        {
            ViewBag.Title = "Статистика входов на сайт";
            ViewBag.Flag = 0;

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Login_stats(DateTime date1, DateTime date2)
        {
            ViewBag.Title = "Статистика входов на сайт";
            ViewBag.Date1 = date1.ToString("dd.MM.yyyy");
            ViewBag.Date2 = date2.ToString("dd.MM.yyyy");

            var DB = new DataContext();
            List<Login_log> logs = DB.Login_logs.ToList();
            List<Login_log> logs_current = new List<Login_log>();

            DateTime end_date = date2.AddDays(1).AddSeconds(-1);

            foreach (var log in logs)
            {
                if (log.log_date.CompareTo(date1) >= 0 && log.log_date.CompareTo(end_date) <= 0)
                {   logs_current.Add(log);  }
            }

            if (logs_current.Count < 1)
            {
                ViewBag.Error = "Нет результатов";
            }

            ViewBag.Logs = logs_current;
            ViewBag.Flag = 1;

            return View();
        }

    }
}
