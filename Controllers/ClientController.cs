﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using System.Security.Cryptography;
using System.Collections.Generic;
using WebMatrix.WebData;
using WebSite.Filters;
using WebSite.Helpers;
using WebSite.Models;
using WebSite.WebServiceReference;
using System.Text;
using System.Text.RegularExpressions;
using SendingEmailwithoutAttachment.Models;
using System.Globalization;

namespace WebSite.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class ClientController : Controller
    {
        private static readonly UsersContext bd = new UsersContext();

        public struct DairyDay
        {
            public DateTime date;
            public String[][] StrLesson;
            public String[][] Notes;
            public String[] hometaskLesson;
            public String klass;
            public String EmployeeId;

            public DairyDay(int WeekDay, DateTime InitDate, Int32 klassId, Int32 Employee)
            {
                klass = klassId.ToString();
                EmployeeId = Employee.ToString();
                var mrc = "";
                var hmt = "";
                hometaskLesson = new string[9];
                StrLesson = new string[9][];
                Notes = new string[5][];
                string WeekTextDay = "";
                switch(WeekDay)
                {
                    case 1: WeekTextDay = "Понедельник"; break;
                    case 2: WeekTextDay = "Вторник"; break;
                    case 3: WeekTextDay = "Среда"; break;
                    case 4: WeekTextDay = "Четверг"; break;
                    case 5: WeekTextDay = "Пятница"; break;
                    case 6: WeekTextDay = "Суббота"; break;
                }
                date = InitDate.AddDays(WeekDay-1);
                var Shedules = WebApiConfig2.WebClient.ПолучитьРасписание(klassId).ToList();
                var Hometask = WebApiConfig2.WebClient.ПолучитьДомашниеЗадания(date, klassId).ToList();
                var q = from t in Shedules
                        where t.ДеньНедели.Наименование == WeekTextDay
                        select new string[2] { t.Предмет.Наименование, t.Предмет.Код.ToString()}.ToArray();
                var less = q.ToArray();

                for (int i = 0; i <= 8; i++)
                    {
                    var Marc = WebApiConfig2.WebClient.ПолучитьОценкуУченикаПоПредметуЗаПериод(Employee, date, date.AddDays(1), klassId).ToList();

                    if (Marc.Count > 0)
                    {
                        mrc = Marc[0].Оценка;
                    }
                    else
                    {
                        mrc = "";
                    }


                    var reqHometask = less.Length > i ? less[i][0] : "";
                    var w = from a in Hometask
                            where a.Предмет.Наименование == reqHometask
                            select a.Задание;
                    hometaskLesson = w.ToArray();

                    if (hometaskLesson.Length > 0)
                    {
                        hmt = hometaskLesson[0];
                    }
                    else
                    {
                        hmt = "";
                    }

                    StrLesson[i] = new string[3]
                        {
                            less.Length > i ? less[i][0] : "",
                            hmt,
                            mrc
                        };
                    }
            }
        }

        public ActionResult DiaryClient(string current)
        {
            var now = new DateTime();
            if (Request.Params["date"] != null)
            {
                now = Convert.ToDateTime(Request.Params["date"]);
            }
            else
            {
                now = DateTime.Now;
            }
            
            int Nowday = (int)now.DayOfWeek;
            now = now.AddDays((Nowday*-1)+1);
            var model = new List<DairyDay>();
            if (Request.Params["klass"] != null) for (int i = 1; i <= 6; i++)
                model.Add(new DairyDay(i, now, Convert.ToInt32(Request.Params["klass"]), Convert.ToInt32(Request.Params["empId"])));
            return View(model);
        }


            public ActionResult Index(string current)
        {
            string error_str = "";

            int current_pupil = (current == null) ? 0 : Int32.Parse(current);

            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(WebSecurity.CurrentUserId);
            
            if (user == null)
                return PartialView("Error");
            
            ViewBag.Client = user.fullname;
            int id = 0;
            bool x = Int32.TryParse(user.ClientID, out id);
            if (x == false)
            {   error_str += "Ошибка: Недопустимый идентификатор клиента."; }
            else
            {
                WebServiceReference2.ИнтернетПользователь profile = null;
                int client_id = 0;

                try
                {
                    profile = WebApiConfig2.WebClient.ПолучитьПрофильПользователя(id);
                }
                catch (System.ServiceModel.FaultException ex)
                {
                    error_str += ex.Message;
                    ViewBag.HomeTasks = null;

                    var DB = new DataContext();
                    var error = new Error();
                    string str = "ClientID: " + user.ClientID;
                    error.Command = "ПолучитьПрофильПользователя " + str;
                    error.Text = ex.Message;
                    error.Date = DateTime.Now;
                    DB.Errors.Add(error);
                    DB.SaveChanges();
                }

                if (profile != null)
                {
                    client_id = profile.Клиент.Код;

                    if (profile.Ученики != null)
                    {

                        ViewBag.Profile = profile;
                        ViewBag.Pupils = profile.Ученики.ToList();

                        WebServiceReference2.Ученик_Строка CurrentPupil = null;

                        if (current_pupil == 0)
                        {
                            CurrentPupil = profile.Ученики.ToList()[0];
                            current_pupil = CurrentPupil.Код;
                        }
                        else
                        {
                            try
                            {
                                CurrentPupil = WebApiConfig2.WebClient.ПолучитьПрофильУченика(current_pupil);
                            }
                            catch (System.ServiceModel.FaultException ex)
                            {
                                error_str += ex.Message;
                                ViewBag.Pupil = null;

                                var DB = new DataContext();
                                var error = new Error();
                                string str = "PupilID: " + current_pupil;
                                error.Command = "ПолучитьПрофильУченика " + str;
                                error.Text = ex.Message;
                                error.Date = DateTime.Now;
                                DB.Errors.Add(error);
                                DB.SaveChanges();
                            }
                        }

                        ViewBag.current_pupil = CurrentPupil;
                        ViewBag.current_pupil_id = CurrentPupil.Код;

                        //ViewBag.img_byte = WebApiConfig2.WebClient.ПолучитьФотоУченика(ViewBag.current_pupil.Код);

                        var Payments = new List<WebServiceReference2.Оплата_Строка>();

                        try
                        {
                            Payments = WebApiConfig2.WebClient.ПолучитьОплату(client_id).ToList();
                        }
                        catch (System.ServiceModel.FaultException ex)
                        {
                            error_str += ex.Message;
                            ViewBag.HomeTasks = null;

                            var DB = new DataContext();
                            var error = new Error();
                            string str = "ClientID: " + client_id;
                            error.Command = "ПолучитьОплату " + str;
                            error.Text = ex.Message;
                            error.Date = DateTime.Now;
                            DB.Errors.Add(error);
                            DB.SaveChanges();
                        }

                        if (Payments != null)
                        {
                            DateTime null_date = DateTime.ParseExact("01.01.0001", "dd.MM.yyyy", CultureInfo.InvariantCulture);
                            DateTime attend_date = null_date;
                            DateTime diary_date = null_date;
                            DateTime hometask_date = null_date;

                            foreach (var item in Payments)
                            {
                                if (item.Ученик.Код == current_pupil)
                                {

                                    if (item.Тариф.Наименование.IndexOf("СМС") != -1)
                                    {
                                        //int months = decimal.ToInt32(item.Сумма / item.Тариф.Стоимость);
                                        //DateTime date = item.Дата.AddMonths(months);
                                        if (item.ДатаОкончания.CompareTo(attend_date) > 0)
                                        { attend_date = item.ДатаОкончания; }
                                    }
                                    else
                                        if (item.Тариф.Наименование.IndexOf("Оценки") != -1)
                                        {
                                            if (item.ДатаОкончания.CompareTo(diary_date) > 0)
                                            { diary_date = item.ДатаОкончания; }
                                        }
                                        else
                                            if (item.Тариф.Наименование.IndexOf("ДЗ") != -1)
                                            {
                                                if (item.ДатаОкончания.CompareTo(hometask_date) > 0)
                                                { hometask_date = item.ДатаОкончания; }
                                            }
                                }
                            }

                            if (attend_date == null_date)
                            { ViewBag.attend_date = "Не оплачено"; }
                            else
                            { ViewBag.attend_date = "до " + attend_date.ToString("dd.MM.yyyy"); }

                            if (diary_date == null_date)
                            { ViewBag.diary_date = "Не оплачено"; }
                            else
                            { ViewBag.diary_date = "до " + diary_date.ToString("dd.MM.yyyy"); }

                            if (hometask_date == null_date)
                            { ViewBag.hometask_date = "Не оплачено"; }
                            else
                            { ViewBag.hometask_date = "до " + hometask_date.ToString("dd.MM.yyyy"); }

                            ViewBag.attend_days = attend_date.Subtract(DateTime.Now).Days;
                            ViewBag.diary_days = diary_date.Subtract(DateTime.Now).Days;
                            ViewBag.hometask_days = hometask_date.Subtract(DateTime.Now).Days;

                            ViewBag.Payments = Payments;
                        }
                    }
                    else
                    {
                        error_str += "Ошибка: Список детей клиента пуст.";
                        ViewBag.current_pupil_id = 0;
                    }
                }
                else
                { error_str += "Ошибка: Профиль клиента пуст."; }
                /*
                var Profile = WebApiConfig.WebClient.GetProfile(id);
                if (Profile.IsError == true)
                {
                    ViewBag.Error = "Ошибка: " + Profile.ErrorMessage;
                    ViewBag.Marks = null;

                    var DB = new DataContext();
                    var error = new Error();
                    string str = "ClientID: " + user.ClientID;
                    error.Command = "GetProfile " + str;
                    error.Text = Profile.ErrorMessage;
                    error.Date = DateTime.Now;
                    DB.Errors.Add(error);
                    DB.SaveChanges();
                }
                else
                {   
                    profile = Profile.Result;
                    ViewBag.Profile = profile;                    
                    ViewBag.img_byte = profile.Pupil.Image;
                
                }
                */
            }

            if (error_str != "")
            { ViewBag.Error += " Ошибка: " + error_str; }
            
            /*            
            var user = Session["user"] as WSUserProfile;
            if (user == null)
                return PartialView("Error");
            var listPays = WebApiConfig.WebClient.GetPays(user.Client.ID);
            ViewBag.Client = user.Client;
            */
            /*
            var DB = new DataContext();
            IEnumerable<Order> orders = DB.Orders.Where(o => o.UserId == user.UserId).Select(s => s);
            
            List<Order> list = new List<Order>();
            foreach (var order in orders)
            {
                if (order.payment_type == "1" || order.payment_type == "4")
                {   list.Add(order);  }                
            }
            ViewBag.Orders = list;
            */
            return View(/* listPays */);
        }

        // GET: /Client/SetChild
        public ActionResult SetChild()
        {
            //ViewBag.Model = new SetChildModel();
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
            ViewBag.Schools = Schools;

            return View();
        }

        // POST: /Client/SetChild
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SetChild(SetChildModel model)
        {
            /*
            var Schools = new List<BaseModel>
            {
                new BaseModel {ID = "1", Name = "Гимназия №8"},
                new BaseModel {ID = "2", Name = "Гимназия №1"}
            };
            ViewBag.Schools = Schools;
            */
            var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
            ViewBag.Schools = Schools;

            if (ModelState.IsValid)
            {
                // запись в базу данных
                var DB = new UsersContext();
                var pupil = new Pupil();

                pupil.School_ID = model.School_ID;
                pupil.Unit_ID = model.Unit_ID;
                pupil.Fullname_1 = model.Fullname_1;
                pupil.Fullname_2 = model.Fullname_2;
                pupil.Fullname_3 = model.Fullname_3;
                pupil.Card_ID = model.Card_ID;
                //pupil.Parent1_ID = WebSecurity.CurrentUserId;

                var user = DB.UserProfiles.Find(WebSecurity.CurrentUserId);// as UserProfile;
                //var user = Session["user"] as UserProfile;
                var child = new Child();
                child.UserProfile = user;
                child.Pupil = pupil;
                DB.Children.Add(child);
                //DB.Children.Attach(child);
                //pupil.Parents = new[] { child };
                //user.Children = new[] { child };

                DB.Pupils.Add(pupil);
                DB.SaveChanges();

                if (user.Active == 1)
                {   return RedirectToAction("Index", "Client"); }

                return RedirectToAction("Activate", "Client");
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            //ModelState.AddModelError("", "Данные указаны неверно.");
            StringBuilder sb = new StringBuilder();
            foreach (ModelState modelState in ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {   sb.Append(error + "\n");    }
            }
            ViewBag.Error = sb.ToString();
            return View(model);
        }

        // GET: /Client/Activate
        public ActionResult Activate()
        {
            var DB = new UsersContext();
            var user = DB.UserProfiles.Find(WebSecurity.CurrentUserId);
            /*
            var children = DB.Children.Where(c => c.UserId == user.UserId).Select(s => s).OrderByDescending(o => o.Pupil_ID);
           
            var pupils = new List<Pupil>();
            foreach (var child in children)
            {
                var pupils_db = DB.Pupils.Where(p => p.Pupil_ID == child.Pupil_ID).Select(s => s).OrderByDescending(o => o.Pupil_ID);
                foreach (var pupil in pupils)
                {   pupils.Add(pupil);  }
            }
            */
            var children = user.Children.ToList();

            if (children.Count > 0)
            {
                string tel = user.Phone;

                string FirstName = user.Fullname_2;
                string LastName = user.Fullname_1;
                double c = double.Parse(children[0].Pupil.Card_ID, CultureInfo.InvariantCulture);
                string card = c.ToString("##0.00000", CultureInfo.InvariantCulture);
                //if (card[0].Equals("0"))
                //{ card = card.Substring(1, card.Length); }

                //int parent = 1;

                string Subject = "Регистрация клиента на сайте idsmart.by";
                string Body = "Добрый день. <br> Это автоматическое сообщение было отправлено потому, что на сайте idsmart.by была произведена попытка регистрации клиента: <br>";
                Body += "ФИО: " + user.fullname + "<br> Email: " + user.Email + "<br> Телефон: " + user.Phone; // +"<br> Школа: " + school;
                Body += "<br> Дата рождения: " + user.Birthdate.ToString("dd.MM.yyyy") + "<br> Имя пользователя: " + user.UserName;
                Body += "<br> Дети: ";

                foreach (var child in children)
                {
                    int school_id = child.Pupil.School_ID;
                    var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
                    string school = Schools.Find(s => s.Код == school_id).Наименование;
                    int unit_id = child.Pupil.Unit_ID;
                    var Units = WebApiConfig2.WebClient.ПолучитьКлассы(school_id).ToList();
                    string unit = Units.Find(s => s.Код == unit_id).Наименование;

                    Body += "<br> ФИО: " + child.Pupil.fullname + "<br> Школа: " + school + "<br> Класс: " + unit;
                }                

                EMail mail = new EMail();
                mail.SendMail("Email", "idsmart2011@ya.ru", new String[] { Subject, Body });
                //mail.SendMail("Email", "alihsey@bk.ru", new String[] { Subject, Body });

                ViewBag.Error = "Запрос принят. Об активации аккаунта Вас уведомят по телефону.";
                /*
                var response = WebApiConfig.WebClient.ActivationProfile(tel, schoolID, unit, FirstName, LastName, card, parent);
                if (response.IsError == true)
                {
                    ViewBag.Error = response.ErrorMessage;

                    var db = new DataContext();
                    var error = new Error();
                    string str = "tel: " + tel + " schoolID: " + schoolID + " unit: " + unit +
                        " pupilFirstName: " + FirstName + " pupilLastName: " + LastName + " card: " + card + " parent: " + parent;
                    error.Command = "ActivationProfile " + str;
                    error.Text = response.ErrorMessage;
                    error.Date = DateTime.Now;
                    db.Errors.Add(error);
                    db.SaveChanges();
                }
                else
                {
                    user.Code = response.Result.Code;
                    user.ClientID = response.Result.ClientID;
                    DB.SaveChanges(); 
                }
                // после тестирования удалить
                ViewBag.Code = user.Code;
                */
            }
            else
            {
                if (user.Parent == 2)
                {
                    /*
                    string tel = user.Phone;
                    int schoolID = Int32.Parse(user.SchoolID);
                    string unit = null;
                    string pupilFirstName = user.Fullname_2;
                    string pupilLastName = user.Fullname_1;
                    string card = null;
                    int parent = 0;
                    
                    var code = WebApiConfig.WebClient.ActivationProfile(tel, schoolID, unit, pupilFirstName, pupilLastName, card, parent);
                    if (code.IsError == true)
                    {
                        ViewBag.Error = code.ErrorMessage;

                        var db = new DataContext();
                        var error = new Error();
                        error.Command = "ActivationProfile";
                        error.Text = code.ErrorMessage;
                        error.Date = DateTime.Now;
                        db.Errors.Add(error);
                        db.SaveChanges();
                    }
                    else
                    {
                        user.Code = code.Result;
                        DB.SaveChanges();
                    }
                    // после тестирования удалить
                    ViewBag.Code = user.Code;
                    */
                    var Schools = WebApiConfig2.WebClient.ПолучитьШколы().ToList();
                    int school_id = Int32.Parse(user.SchoolID);
                    string school = Schools.Find(s => s.Код == school_id).Наименование;

                    string Subject = "Регистрация педагога на сайте idsmart.by";
                    string Body = "Добрый день. <br> Это автоматическое сообщение было отправлено потому, что на сайте idsmart.by была произведена попытка регистрации педагога: <br>";
                    Body += "ФИО: " + user.fullname + "<br> Email: " + user.Email + "<br> Телефон: " + user.Phone + "<br> Школа: " + school;
                    Body += "<br> Дата рождения: " + user.Birthdate.ToString("dd.MM.yyyy") + "<br> Имя пользователя: " + user.UserName;

                    EMail mail = new EMail();
                    mail.SendMail("Email", "idsmart2011@ya.ru", new String[] { Subject, Body });
                    //mail.SendMail("Email", "alihsey@bk.ru", new String[] { Subject, Body });

                    ViewBag.Error = "Запрос принят. Об активации аккаунта Вас уведомят по телефону.";
                }
                else
                {
                    ViewBag.Error = "Ошибка: дети не найдены. Внесите своего ребенка в базу данных в личном кабинете";
                }
            }
            return View();
        }

        // POST: /Client/Activate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Activate(int Code)
        {
            var DB = new UsersContext();
            var user = DB.UserProfiles.Find(WebSecurity.CurrentUserId);

            if (Code != 0 && Code == user.Code)
            {
                user.Active = 1;
                DB.SaveChanges();
                if (user.Parent == 2)
                {
                    Roles.AddUserToRole(user.UserName, "teacher");
                    return RedirectToAction("Index", "Home");
                }

                Roles.AddUserToRole(user.UserName, "clients");
                return RedirectToAction("Index", "Client");
            }

            // Появление этого сообщения означает наличие ошибки; повторное отображение формы
            ModelState.AddModelError("", "Данные указаны неверно.");
            return View();
        }

        public ActionResult Cart()
        {
            var DB = new UsersContext();
            var user = DB.UserProfiles.Find(WebSecurity.CurrentUserId);
            var children = DB.Children.Where(c => c.UserId == user.UserId).Select(s => s).OrderByDescending(o => o.Pupil_ID);

            var pupils = new List<Pupil>();
            foreach (var child in children)
            {
                var pupil = DB.Pupils.Where(p => p.Pupil_ID == child.Pupil_ID).Select(s => s).OrderByDescending(o => o.Pupil_ID);
                foreach (var Pupil in pupils)
                {   pupils.Add(Pupil);  }
            }

            if (pupils.Count > 0)
            {   ViewBag.Children = pupils;  }

            ViewBag.Email = user.Email;
            ViewBag.Phone = user.Phone;

            var goods = new List<GoodsModel>
            {
                new GoodsModel { ID = 0, Name = "", Price = "0" },
                new GoodsModel { ID = 1, Name = "СМС Начальный", Price = "20000" },
                new GoodsModel { ID = 2, Name = "СМС Базовый", Price = "30000" },
                //new GoodsModel { ID = 3, Name = "Оценки Начальный", Price = "35000" },
                new GoodsModel { ID = 4, Name = "Оценки Базовый", Price = "50000" },
                //new GoodsModel { ID = 5, Name = "Д/З Начальный", Price = "15000" },
                new GoodsModel { ID = 6, Name = "Д/З Базовый", Price = "30000" },
                new GoodsModel { ID = 7, Name = "Новая карта доступа", Price = "60000" },
                new GoodsModel { ID = 8, Name = "Восстановление карты", Price = "30000" },
                new GoodsModel { ID = 9, Name = "Новая наклейка", Price = "10000" }
            };
            ViewBag.GoodsList = goods;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PaySubmit(string phone, string email, int[] goods_id, int[] months)
        {
            ViewBag.test = "1";
            ViewBag.email = email;
            ViewBag.phone = phone;

            var goods = new List<GoodsModel>();
            int total = 0;

            int i = 0;
            string str_goods = "";
            foreach (int id in goods_id)
            {
                switch (id)
                {
                    case 0: break;
                    case 1:
                        goods.Add(new GoodsModel { ID = id, Name = "СМС Начальный", Price = "20000", Months = months[i] });
                        str_goods += "СМС Начальный, 20000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 20000 * months[i]; break;
                    case 2:
                        goods.Add(new GoodsModel { ID = id, Name = "СМС Базовый", Price = "30000", Months = months[i] });
                        str_goods += "СМС Базовый, 30000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 30000 * months[i]; break;
                    case 3:
                        goods.Add(new GoodsModel { ID = id, Name = "Оценки Начальный", Price = "35000", Months = months[i] });
                        str_goods += "Оценки Начальный, 35000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 35000 * months[i]; break;
                    case 4:
                        goods.Add(new GoodsModel { ID = id, Name = "Оценки Базовый", Price = "50000", Months = months[i] });
                        str_goods += "Оценки Базовый, 40000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 40000 * months[i]; break;
                    case 5:
                        goods.Add(new GoodsModel { ID = id, Name = "Д/З Начальный", Price = "15000", Months = months[i] });
                        str_goods += "Д/З Начальный, 15000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 15000 * months[i]; break;
                    case 6:
                        goods.Add(new GoodsModel { ID = id, Name = "Д/З Базовый", Price = "30000", Months = months[i] });
                        str_goods += "Д/З Базовый, 20000 руб. * " + months[i].ToString() + " месяцев;   ";
                        total += 20000 * months[i]; break;
                    case 7:
                        goods.Add(new GoodsModel { ID = id, Name = "Новая карта доступа", Price = "60000", Months = 1 });
                        str_goods += "Новая карта доступа, 50000 руб.;   ";
                        total += 50000; break;
                    case 8:
                        goods.Add(new GoodsModel { ID = id, Name = "Восстановление карты", Price = "30000", Months = 1 });
                        str_goods += "Восстановление карты, 30000 руб.;   ";
                        total += 30000; break;
                    case 9:
                        goods.Add(new GoodsModel { ID = id, Name = "Новая наклейка", Price = "10000", Months = 1 });
                        str_goods += "Новая наклейка, 10000 руб.;   ";
                        total += 10000; break;
                }
                i++;
            }

            ViewBag.SelectedGoodsList = goods;
            ViewBag.total_price = total;


            var DB = new DataContext();
            var order = new Order();
            order.phone = phone;
            order.email = email;
            order.date = DateTime.Now;
            order.goods = str_goods;
            order.UserId = WebSecurity.CurrentUserId;
            DB.Orders.Add(order);
            DB.SaveChanges();

            ViewBag.Order_id = order.ID;
            ViewBag.seed = order.date.ToString("ssmmHHddMMyy");

            // подсчет контрольной суммы
            string str = ViewBag.seed + "249227470" + order.ID + ViewBag.test + "BYR" + total + "2cVkxpYfcp";
            byte[] bytes = Encoding.ASCII.GetBytes(str);
            byte[] hashBytes = SHA1.Create().ComputeHash(bytes);
            var sb = new StringBuilder();
            foreach (byte b in hashBytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            ViewBag.signature = sb.ToString();

            //return RedirectPermanent("https://secure.sandbox.webpay.by:8843");
            return View();
        }

        // GET: /Client/Stats_pays
        public ActionResult Stats_pays(int client_id, int pupil_id)
        {
            var Payments = new List<WebServiceReference2.Оплата_Строка>();

            try
            {
                Payments = WebApiConfig2.WebClient.ПолучитьОплату(client_id).ToList();
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "ClientID: " + client_id;
                error.Command = "ПолучитьОплату " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            
            if (Payments.Count > 0)
            {
                for (int i = Payments.Count - 1; i >= 0; i--)
                {
                    if (Payments[i].Ученик.Код != pupil_id)
                    { Payments.RemoveAt(i); }
                }

                Payments.Sort((x, y) => x.Дата.CompareTo(y.Дата));
            }
            ViewBag.Payments = Payments;

            return View();
        }

        // GET: /Client/Attend
        public ActionResult Attend(int pupil_id)
        {
        //public ActionResult Stats_stuff_short(string date1, string date2, int school, string FIO)

            string error_str = "";

            int unit_id = 0;
            int school_id = 0;

            WebServiceReference2.Ученик_Строка Pupil = null;

            try
            {
                Pupil = WebApiConfig2.WebClient.ПолучитьПрофильУченика(pupil_id);
                ViewBag.Pupil = Pupil;

                unit_id = Pupil.Класс.Код;
                school_id = Pupil.Школа.Код;
            }
            catch (System.ServiceModel.FaultException ex)
            {
                error_str += ex.Message;
                ViewBag.Pupil = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "PupilID: " + pupil_id;
                error.Command = "ПолучитьПрофильУченика " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            var Date2 = DateTime.Now;
            var sub = TimeSpan.FromDays(33);
            var Date1 = Date2.Subtract(sub);
            
            // костыль для каникул
            /*
            sub = TimeSpan.FromDays(99);
            Date1 = Date1.Subtract(sub);
            Date2 = Date2.Subtract(sub);
            */
            ViewBag.Date1 = Date1;
            ViewBag.Date2 = Date2;
            
            var report = WebApiConfig.WebClient.ReportByVisitsOfPupils(Date1, Date2, school_id, unit_id);
            if (report.IsError == true)
            {
                ViewBag.Error = report.ErrorMessage;
                ViewBag.Report = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "date1: " + Date1.ToString("dd.MM.yyyy") + " date2: " + Date2.ToString("dd.MM.yyyy") + " school: " + school_id.ToString() + " unit: " + unit_id;
                error.Command = "ReportByVisitsOfPupils " + str;
                error.Text = report.ErrorMessage;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            else
            {
                var Reports = report.Result;
                if (Reports.Count > 0)
                {
                    Reports.Sort((x, y) => x.Date.CompareTo(y.Date));
                    //int i = 0;
                    //var Reports_all = new List<Report_attend>();
                    /*
                    foreach (var row in Reports)
                    {
                        if (FIO == "" || FIO == row.EmployeeFIO)
                        {
                            string d = row.Date.ToString("dd MMMM yyyy");
                            string time = row.Date.ToString("HH:mm:ss");
                            var NewTime = DateTime.ParseExact(time, "HH:mm:ss", CultureInfo.InvariantCulture);
                            Report_stuff old_val = null;
                            string post = row.AdditionInformation;

                            if (d != Date && d != "01 января 0001")
                            {
                                Dates.Add(d);
                                Date = d;
                                i++;
                                Reports_day.Sort((x, y) => x.Name.CompareTo(y.Name));
                                for (int r = Reports_day.Count - 1; r >= 0; r--)
                                {
                                    if (Reports_day[r].Post == "IDSmart")
                                    { Reports_day.RemoveAt(r); }
                                }
                                Reports_all.Add(Reports_day);
                                Reports_day = new List<Report_stuff>();
                            }
                            if (row.Way == 1 && post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = time, Leave = "--" }); }
                            else if (row.Way == 2 && post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            {
                                if (Reports_day.Count > 0)
                                { old_val = Reports_day.FindLast(r => r.Name == row.EmployeeFIO); }
                                if (old_val != null)
                                {
                                    var new_val = new Report_stuff { Name = old_val.Name, Post = old_val.Post, Come = old_val.Come, Leave = time };
                                    int n = Reports_day.IndexOf(old_val);

                                    if (old_val.Leave != "--")
                                    {
                                        var OldTime = DateTime.ParseExact(old_val.Leave, "HH:mm:ss", CultureInfo.InvariantCulture);
                                        if (DateTime.Compare(OldTime, NewTime) < 0 && n != -1)
                                        { Reports_day[n] = new_val; }
                                    }
                                    else if (n != -1)
                                    { Reports_day[n] = new_val; }
                                }
                                else
                                { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = "--", Leave = time }); }
                            }
                            else if (post.IndexOf("НАДОМНИК") == -1 && post.IndexOf("Пассив") == -1)
                            { Reports_day.Add(new Report_stuff { Name = row.EmployeeFIO, Post = post, Come = "--", Leave = "--" }); }

                            var first_val = Reports_day.Find(r => r.Name == row.EmployeeFIO);
                            var last_val = Reports_day.FindLast(r => r.Name == row.EmployeeFIO);
                            if (first_val != null && last_val != null && first_val != last_val)
                            {
                                if (first_val.Come != "--" || last_val.Leave != "--")
                                {
                                    var new_val = new Report_stuff { Name = first_val.Name, Post = first_val.Post, Come = first_val.Come, Leave = last_val.Leave };
                                    var n = Reports_day.IndexOf(first_val);
                                    Reports_day[n] = new_val;
                                    n = Reports_day.IndexOf(last_val);
                                    Reports_day.RemoveAt(n);
                                }
                            }                            
                        }
                    }
                     
                    Reports_day.Sort((x, y) => x.Name.CompareTo(y.Name));
                    for (int r = Reports_day.Count - 1; r >= 0; r--)
                    {
                        if (Reports_day[r].Post == "IDSmart")
                        { Reports_day.RemoveAt(r); }
                    }
                    Reports_all.Add(Reports_day);

                    ViewBag.Dates = Dates;
                    */
                    ViewBag.Report = Reports;
                    
                }
                else
                { ViewBag.Error = "Нет результатов."; }
            }

            return View();
        }

        // GET: /Client/Diary
        public ActionResult Diary(int pupil_id, string quarter)
        {
            string error_str = "";

            int unit_id = 0;
            int school_id = 0;

            WebServiceReference2.Ученик_Строка Pupil = null;

            try
            {
                Pupil = WebApiConfig2.WebClient.ПолучитьПрофильУченика(pupil_id);
                ViewBag.Pupil = Pupil;

                unit_id = Pupil.Класс.Код;
                school_id = Pupil.Школа.Код;
            }
            catch (System.ServiceModel.FaultException ex)
            {
                error_str += ex.Message;
                ViewBag.Pupil = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "PupilID: " + pupil_id;
                error.Command = "ПолучитьПрофильУченика " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            var Quarters = new List<WebServiceReference2.Четверть_Строка>();

            try
            {
                Quarters = WebApiConfig2.WebClient.ПолучитьЧетверти(school_id).ToList();
            }
            catch (System.ServiceModel.FaultException ex)
            {
                error_str += ex.Message;
                ViewBag.Pupil = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "SchoolID: " + school_id;
                error.Command = "ПолучитьЧетверти " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            /*
            string quarter = "";
            
            List<Quarter> Quarters = null;
            if (school_id == 2 || school_id == 11)
            {
                Quarters = new List<Quarter>
                {
                    new Quarter {ID = 0, Name = "1й триместр", Date1 = "01.09.2015", Date2 = "30.11.2015" },
                    new Quarter {ID = 1, Name = "2й триместр", Date1 = "01.12.2015", Date2 = "29.02.2016" },
                    new Quarter {ID = 2, Name = "3й триместр", Date1 = "01.03.2016", Date2 = "30.05.2016" },
                };
            }
            else
            {
                Quarters = new List<Quarter>
                {
                    new Quarter {ID = 0, Name = "1-я четверть", Date1 = "01.09.2015", Date2 = "25.10.2015" },
                    new Quarter {ID = 1, Name = "2-я четверть", Date1 = "02.11.2015", Date2 = "27.12.2015" },
                    new Quarter {ID = 2, Name = "3-я четверть", Date1 = "11.01.2016", Date2 = "20.03.2016" },
                    new Quarter {ID = 3, Name = "4-я четверть", Date1 = "28.03.2016", Date2 = "31.05.2016" },
                };
            }
            */
            ViewBag.Quarter = Quarters;

            if (quarter == null)
            {
                if (DateTime.Now.CompareTo(Quarters[1].НачалоЧетверти) <= 0)
                { quarter = "1"; }
                else if (DateTime.Now.CompareTo(Quarters[2].НачалоЧетверти) <= 0)
                { quarter = "2"; }
                else if (Quarters.Count > 3)
                {
                    if (DateTime.Now.CompareTo(Quarters[3].НачалоЧетверти) <= 0)
                    { quarter = "3"; }
                    else
                    { quarter = "4"; }
                }
                else
                { quarter = "3"; }
            }

            int quarter_id = Int32.Parse(quarter);
            ViewBag.QuarterSel = quarter;

            var Subjects = new List<WebServiceReference2.Предмет_Строка>();
            try
            {
                Subjects = WebApiConfig2.WebClient.ПолучитьПредметыКласса(unit_id).ToList();
                ViewBag.Subjects = Subjects;
                if (Subjects.Count == 0)
                { error_str += "Пустой список предметов. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьПредметыКласса " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            List<DateTime> Days = new List<DateTime>();
            var Months = new List<Month>();
            var BeginDay = Quarters[quarter_id-1].НачалоЧетверти;
            var EndDay = Quarters[quarter_id-1].КонецЧетверти;
            //var start_date = DateTime.Now.AddDays(7);
            
            for (int i = 333; i >= 0; i--)
            {
                var sub = TimeSpan.FromDays(i);
                DateTime day = EndDay.Subtract(sub);
                if (DateTime.Compare(day, BeginDay) >= 0 && day.DayOfWeek != DayOfWeek.Sunday && day.DayOfWeek != DayOfWeek.Saturday)   // заменить на проверку по расписанию
                { Days.Add(day); }
                if (DateTime.Compare(day, EndDay) >= 0)
                { break; }
            }
            /*
            var TimeTables = new List<WebServiceReference2.Расписание_Строка>();
            try
            {
                TimeTables = WebApiConfig2.WebClient.ПолучитьРасписание(unit_id).ToList();
                ViewBag.TT = TimeTables;
                if (TimeTables.Count == 0)
                { error_str += "Расписание пустое. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьРасписание " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            for (int i = Days.Count - 1; i >= 0; i--)
            {
                string day_str = WeekDay(Days[i]);
                int flag = 0;
                foreach (var item in TimeTables)
                {
                    if (item.Предмет.Код == subject && item.ДеньНедели.Наименование == day_str)
                    { flag++; }
                }
                if (flag == 0)
                { Days.RemoveAt(i); }
            }

            var Pupils = new List<WebServiceReference2.Ученик_Строка>();
            try
            {
                Pupils = WebApiConfig2.WebClient.ПолучитьУчеников(unit_id).ToList();
                ViewBag.Pupils = Pupils;
                if (Pupils.Count == 0)
                { error_str += "Список учеников пустой. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьУчеников " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            */
            if (Subjects.Count > 0 && Days.Count > 0)
            {
                var Marks_arr = new List<List<WebServiceReference2.Оценка_Строка>>();
                foreach (var subject in Subjects)
                {
                    try
                    {
                        var Marks = WebApiConfig2.WebClient.ПолучитьОценкуУченикаПоПредметуЗаПериод(pupil_id, Days.First(), Days.Last(), subject.Код);
                        Marks_arr.Add(Marks.ToList());
                    }
                    catch (System.ServiceModel.FaultException ex)
                    {
                        ViewBag.Error = ex.Message;
                        ViewBag.HomeTasks = null;

                        var DB = new DataContext();
                        var error = new Error();
                        string str = "subject: " + subject.Код.ToString() + " pupil: " + pupil_id.ToString() +
                           " date1: " + Days.First().ToString("dd.MM.yyyy") + " date2: " + Days.Last().ToString("dd.MM.yyyy");
                        error.Command = "ПолучитьОценкуУченикаПоПредметуЗаПериод " + str;
                        error.Text = ex.Message;
                        error.Date = DateTime.Now;
                        DB.Errors.Add(error);
                        DB.SaveChanges();
                    }
                }
                ViewBag.Marks = Marks_arr;

                var Fresh_arr = new List<List<int>>();
                int a = 0;
                foreach (var subject in Subjects)
                {
                    var Fresh = new List<int>();
                    int b = 0;
                    foreach (var day in Days)
                    {
                        int flag = 0;

                        foreach (var mark in Marks_arr[a])
                        {
                            if (day.ToString("dd.MM.yyyy") == mark.Период.ToString("dd.MM.yyyy"))
                            {
                                if (mark.ДатаСохранения > DateTime.Now.Subtract(TimeSpan.FromDays(7)))
                                { flag = 1; }
                            }
                        }

                        if (flag == 1)
                        { Fresh.Add(1); }
                        else
                        { Fresh.Add(0); }
                        b++;
                    }
                    Fresh_arr.Add(Fresh.ToList());
                    a++;
                }
                ViewBag.Fresh_arr = Fresh_arr;

                string month = Days[0].ToString("MMMM");
                int span = 0;
                foreach (var day in Days)
                {
                    var m = day.ToString("MMMM");
                    if (m != month)
                    {
                        Months.Add(new Month { span = span, name = month });
                        month = m;
                        span = 0;
                    }
                    span++;
                }
                Months.Add(new Month { span = span, name = month });
            }

            ViewBag.Days = Days;
            ViewBag.Months = Months;
            if (error_str != "")
            { ViewBag.Error += " Ошибка: " + error_str; }
            
            return View();
        }

        // GET: /Client/Hometask
        public ActionResult Hometask(int pupil_id)
        {
            string error_str = "";

            int unit_id = 0;
            int school_id = 0;

            WebServiceReference2.Ученик_Строка Pupil = null;

            try
            {
                Pupil = WebApiConfig2.WebClient.ПолучитьПрофильУченика(pupil_id);
                ViewBag.Pupil = Pupil;

                unit_id = Pupil.Класс.Код;
                school_id = Pupil.Школа.Код;

                ViewBag.School = Pupil.Школа.Наименование;
                ViewBag.Klass = Pupil.Класс.Наименование;
            }
            catch (System.ServiceModel.FaultException ex)
            {
                error_str += ex.Message;
                ViewBag.Pupil = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "PupilID: " + pupil_id;
                error.Command = "ПолучитьПрофильУченика " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            //var sub = TimeSpan.FromDays(7);
            //var BeginDay = DateTime.Now.Subtract(sub);
            DateTime now = DateTime.Now;

            var Quarters = new List<WebServiceReference2.Четверть_Строка>();

            try
            {
                Quarters = WebApiConfig2.WebClient.ПолучитьЧетверти(school_id).ToList();
            }
            catch (System.ServiceModel.FaultException ex)
            {
                error_str += ex.Message;
                ViewBag.Pupil = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "SchoolID: " + school_id;
                error.Command = "ПолучитьЧетверти " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }

            int quarter = -1;

            if (now.CompareTo(Quarters[1].НачалоЧетверти) <= 0)
            { quarter = 0; }
            else if (now.CompareTo(Quarters[2].НачалоЧетверти) <= 0)
            { quarter = 1; }
            else if (Quarters.Count > 3)
            {
                if (now.CompareTo(Quarters[3].НачалоЧетверти) <= 0)
                { quarter = 2; }
                else
                { quarter = 3; }
            }
            else
            { quarter = 2; }

            DateTime QEndDay = Quarters[quarter].КонецЧетверти;

            if (QEndDay.CompareTo(now) < 0)
            { now = QEndDay; }

            var BeginDay = now.AddDays(-7);

            int add = 1;
            if (now.DayOfWeek == DayOfWeek.Friday)
            { add = 3; }
            else if (now.DayOfWeek == DayOfWeek.Saturday)
            { add = 2; }
            var EndDay = now.AddDays(add);

            // --------- костыль под каникулы --------------
            /*
            var sub100 = TimeSpan.FromDays(100);
            BeginDay = BeginDay.Subtract(sub100);
            EndDay = EndDay.Subtract(sub100);
            */

            List<DateTime> Days = new List<DateTime>();

            for (int i = 12; i >= 0; i--)
            {
                var sub = TimeSpan.FromDays(i);
                DateTime day = EndDay.Subtract(sub);
                if (DateTime.Compare(day, BeginDay) >= 0 && day.DayOfWeek != DayOfWeek.Sunday && day.DayOfWeek != DayOfWeek.Saturday)
                { Days.Add(day); }
            }
            ViewBag.Days = Days;

            var Subjects = new List<WebServiceReference2.Предмет_Строка>();
            try
            {
                Subjects = WebApiConfig2.WebClient.ПолучитьПредметыКласса(unit_id).ToList();
                ViewBag.Subjects = Subjects;
                if (Subjects.Count == 0)
                { error_str += "Пустой список предметов. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьПредметыКласса " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }
            ViewBag.Subjects = Subjects;

            var TimeTable = new List<WebServiceReference2.Расписание_Строка>();
            try
            {
                TimeTable = WebApiConfig2.WebClient.ПолучитьРасписание(unit_id).ToList();
                if (TimeTable.Count == 0)
                { error_str += "Расписание пустое. "; }
            }
            catch (System.ServiceModel.FaultException ex)
            {
                ViewBag.Error = ex.Message;
                ViewBag.HomeTasks = null;

                var DB = new DataContext();
                var error = new Error();
                string str = "unit_id: " + unit_id.ToString();
                error.Command = "ПолучитьРасписание " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();
            }            
            /*
            ViewBag.Date = date;
            ViewBag.UnitID = unit_id;
            ViewBag.Klass = klass;
            ViewBag.School = school;
            ViewBag.Flag = 1;
            */

            var HomeTasks = new List<List<WebServiceReference2.ДЗ_Строка>>();
            var TimeTables = new List<List<WebServiceReference2.Расписание_Строка>>();
            foreach (var date in Days)
            {
                try
                {
                    var HomeTask = WebApiConfig2.WebClient.ПолучитьДомашниеЗадания(date, unit_id).ToList();
                    HomeTasks.Add(HomeTask);
                }
                catch (System.ServiceModel.FaultException ex)
                {
                    ViewBag.Error = ex.Message;

                    var DB = new DataContext();
                    var error = new Error();
                    string str = "date: " + date.ToString("dd.MM.yyyy") + " unit_id: " + unit_id.ToString();
                    error.Command = "ПолучитьДомашниеЗадания " + str;
                    error.Text = ex.Message;
                    error.Date = DateTime.Now;
                    DB.Errors.Add(error);
                    DB.SaveChanges();
                }

                var TimeTable_day = new List<WebServiceReference2.Расписание_Строка>();
                foreach (var TT in TimeTable)
                {
                    if (TT.ДеньНедели.Наименование == WeekDay(date))
                        TimeTable_day.Add(TT);
                }
                TimeTables.Add(TimeTable_day);
            }

            if (HomeTasks.Count > 0)
            { ViewBag.HomeTasks = HomeTasks; }

            if (TimeTables.Count > 0)
            { ViewBag.TT = TimeTables; }

            return View();
        }

        // POST: /Client/GetUnits
        [HttpPost]
        public ActionResult GetUnits(int school)
        {
            var Units = WebApiConfig2.WebClient.ПолучитьКлассы(school).ToList();
            var Классы = new List<WebServiceReference2.Класс_Строка>();
            for (int i = 1; i <= 11; i++)
            {
                foreach (var unit in Units)
                {
                    var regex = new Regex(@"^" + i.ToString() + @"\D.+$");
                    if (regex.IsMatch(unit.Наименование))
                    { Классы.Add(unit); }
                }
            }
            ViewBag.Units = Классы;

            return PartialView("GetUnits");
        }

        // Post: /Client/EditDiary
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditDiary(int name, int pk, string value, string date, string comment)
        {
            var regex = new Regex(@"^(10|\d|н|Н)((\/|\\)(10|\d))?$");
            if (regex.IsMatch(value) == false && value != "")
            { return new HttpStatusCodeResult(415); }

            return new HttpStatusCodeResult(200);
        }

        public ActionResult Pupil(int pupilID)
        {
            var user = Session["user"] as WSUserProfile;
            if (user == null)
                return PartialView("Error");

            var pupil = user.Client.Pupils.FirstOrDefault(s => s.ID == pupilID);
            if (pupil == null)
                return PartialView("Error");

            ViewBag.Client = user.Client;
            ViewBag.Pupil = pupil;
            Session["Pupil"] = pupil;

            return View(pupil);
        }

        public ActionResult PupilPhoto()
        {
            var pupil = Session["Pupil"] as WSPupil;
            if (pupil == null)
                return null;

            return File(pupil.Image, "image/jpeg");
        }

        public ActionResult Task(string date)
        {
            //var userName = Session["UserName"] as string;
            //if (string.IsNullOrEmpty(userName))
            //{
            //    return null;
            //}

            var d = DateTime.Now;

            if (!DateTime.TryParse(date, out d))
                return null;

            var pupil = Session["Pupil"] as WSPupil;
            if (pupil == null) return null;

            ViewBag.Date = d;

            return PartialView(null);
        }

        public ActionResult Balls(double start, double end)
        {
            return null;
        }

        public ActionResult QuarterAllBalls()
        {
            return null;
        }

        private static DateTime ConvertFromUnixTimestamp(double timestamp)
        {
            var origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return origin.AddSeconds(timestamp);
        }

        private static long ConvertToUnixTimestamp(DateTime date)
        {
            TimeSpan tspan = date.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0));

            return (long)Math.Truncate(tspan.TotalSeconds);
        }

        public FileContentResult getImg(byte[] img_byte)
        {
            return (img_byte != null) ? File(img_byte, "image/bmp") : null;
            //return (img_byte != null) ? new FileContentResult(img_byte, "image/jpeg") : null;
        }

        public string WeekDay(DateTime day)
        {
            switch (day.DayOfWeek.ToString())
            {
                case "Monday": return "Понедельник";
                case "Tuesday": return "Вторник";
                case "Wednesday": return "Среда";
                case "Thursday": return "Четверг";
                case "Friday": return "Пятница";
                case "Saturday": return "Суббота";
            }
            return "Воскресенье";
        }

        public class Quarter
        {
            public int ID { get; set; }
            public string Name { get; set; }
            public string Date1 { get; set; }
            public string Date2 { get; set; }
        }

        public class Month
        {
            public int span { get; set; }
            public string name { get; set; }
        }

    }

    public class CalendarDTO
    {
        public int id { get; set; }
        public string title { get; set; }
        public long start { get; set; }
        public long end { get; set; }
        public bool allDay { get; set; }
        public string description { get; set; }
    }
}
