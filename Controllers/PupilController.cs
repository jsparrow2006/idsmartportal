﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebSite.Filters;
using WebSite.WebServiceReference;

namespace WebSite.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class PupilController : Controller
    {
        //
        // GET: /Pupil/

        public ActionResult Index(/* int pupilID */)
        {
            /*
            var user = Session["user"] as WSUserProfile;
            if (user == null)
                return PartialView("Error");
            
            var pupil = user.Client.Pupils.FirstOrDefault(s => s.ID == pupilID);
            if (pupil == null)
                return PartialView("Error");
            
            //ViewBag.Client = client;
            //ViewBag.Pupil = pupil;
            Session["Pupil"] = pupil;

            var d = DateTime.Now;

            var model = new PupilControllerModel
                        {
                            Client = user.Client,
                            Pupil = pupil
                        };
            
            return View(model);
             */
            return View();
        }

        public ActionResult PupilPhoto()
        {
            var pupil = Session["Pupil"] as WSPupil;
            if (pupil == null)
                return null;

            return File(pupil.Image, "image/jpeg");
        }
    }

    public class PupilControllerModel
    {
        public WSClient Client { get; set; }
        public WSPupil Pupil { get; set; }
        //public List<WSTask> Tasks { get; set; }
    }
}
