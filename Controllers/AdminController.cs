﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using DotNetOpenAuth.AspNet;
using Microsoft.Web.WebPages.OAuth;
using System.Text.RegularExpressions;
using System.Globalization;
using Newtonsoft.Json;
using WebMatrix.WebData;
using WebSite.Filters;
using WebSite.Models;

namespace WebSite.Controllers
{
    [Authorize(Roles = "admins")]
    [InitializeSimpleMembership]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        public ActionResult Index(string msg)
        {
            ViewBag.message = msg;

            var list = new List<AdminControllerUserModel>();
            var userContext = new UsersContext();
            var users = userContext.UserProfiles.ToList();
            users.Sort((x, y) =>
            {
                return x.UserId.CompareTo(y.UserId);
            });

            foreach (var user in users)
            {
                var roles = Roles.GetRolesForUser(user.UserName).Aggregate("", (current, s) => current + (s + ","));

                if (roles.Length > 0)
                    roles = roles.Remove(roles.Length - 1, 1);

                list.Add(new AdminControllerUserModel
                             {
                                 UserName = user.UserName,
                                 Fullname = user.fullname,
                                 Roles = roles,
                                 Active = user.Active,
                                 UserId = user.UserId,
                                 Parent = user.Parent,
                                 Schools = user.SchoolID,
                                 Units = user.Units,
                                 ClientId = user.ClientID
                             });
            }

            return View(list);
        }

        //
        // GET: /Admin/ResetPassword
        public ActionResult ResetPassword(string user_name, string error)
        {
            //ViewBag.HasLocalPassword = OAuthWebSecurity.HasLocalAccount(user_id);
            //var userContext = new UsersContext();


            if (error != null)
            {
                ViewBag.error = error;
            }

            return View();
        }

        //
        // POST: /Admin/ResetPassword
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)//(string user_name, string new_password)
        {            
            //var userContext = new UsersContext();
            bool hasLocalAccount = OAuthWebSecurity.HasLocalAccount(WebSecurity.GetUserId(model.UserName));

            if (hasLocalAccount)
            {
                // В ряде случаев при сбое ResetPassword породит исключение, а не вернет false.
                bool changePasswordSucceeded;

                //MvcMembership

                string token = WebSecurity.GeneratePasswordResetToken(model.UserName, 10);

                try
                {   changePasswordSucceeded = WebSecurity.ResetPassword(token, model.NewPassword);   }
                catch (Exception)
                {   changePasswordSucceeded = false;  }

                if (changePasswordSucceeded)
                {   return RedirectToAction("ResetPasswordSuccess");  }
                else
                {   return RedirectToAction("ResetPassword", "Admin", new { error = "Неправильный текущий пароль или недопустимый новый пароль." });  }
            }
            else
            {   return RedirectToAction("ResetPassword", "Admin", new { error = "У пользователя отсутствует локальная учетная запись." });  }
                
            //return View();
        }

        //
        // GET: /Admin/ResetPasswordSuccess
        public ActionResult ResetPasswordSuccess()
        {
            
            return View();
        }

        public ActionResult GetRoles()
        {
            var result = Roles.GetAllRoles().Select(role => new xEditableItem {value = role, text = role}).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveRole(string name, string pk, List<string> value)
        {
            try
            {
                if (Roles.IsUserInRole(name,"users"))
                {
                    Roles.RemoveUserFromRoles(name, Roles.GetRolesForUser(name));
                }

                var roles = Roles.GetRolesForUser(name);

                if (value != null)
                {
                    foreach (var v in value)
                    {
                        Roles.AddUserToRole(name, v);
                    }
                }
                /*
                foreach (var v in value.Where(v => !roles.Contains(v)))
                {
                    Roles.AddUserToRole(name, v);
                }
                */
                return Json(new { success = true });
            }
            catch (Exception exception)
            {
                return Json(new { success = false, msg = exception.Message });
            }
        }

        // Get: /Admin/DeleteUser
        public ActionResult DeleteUser(string username)
        {
            ViewBag.Message = "Удалить пользователя";

            int user_id = WebSecurity.GetUserId(username);

            var userDB = new UsersContext();
            var user = userDB.UserProfiles.Find(user_id);

            ViewBag.User = user;

            return View();
        }

        // Post: /Admin/DeleteUser
        [HttpPost]
        public ActionResult DeleteUser(string username, int user_id)
        {
            try
            {
                if (Roles.IsUserInRole(username, "users"))
                {
                    Roles.RemoveUserFromRoles(username, Roles.GetRolesForUser(username));
                }
                
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(user_id);
                userDB.UserProfiles.Remove(user);
                userDB.SaveChanges();

                return RedirectToAction("Index", "Admin", new { msg = "Пользователь успешно удален" });
            }
            catch (Exception exception)
            {
                return RedirectToAction("Index", "Admin", new { msg = "Ошибка удаления: " + exception.Message });
            }
        }
        
        // Post: /Admin/EditUser
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditUser(string name, int pk, string value, string comment)
        {
            Regex regex = null;

            switch (name)
            {
                case "Schools":
                    regex = new Regex(@"^((\d){1,6}(, | |,))*((\d){1,6})$");
                    break;
                case "Units":
                    regex = new Regex(@"^((\d){1,6}(, | |,))*((\d){1,6})$");
                    break;
                case "Active":
                    regex = new Regex(@"^(0|1)$");
                    break;
                case "Parent":
                    regex = new Regex(@"^(1|2)$");
                    break;
                case "ClientId":
                    regex = new Regex(@"^(\d){1,6}$");
                    break;
            }

            if (regex.IsMatch(value) == false && value != "")
            { 
                return new HttpStatusCodeResult(415);
            }

            try
            {
                var userDB = new UsersContext();
                var user = userDB.UserProfiles.Find(pk);

                switch (name)
                {
                    case "Schools":
                        user.SchoolID = value;
                        break;
                    case "Units":
                        user.Units = value;
                        break;
                    case "Active":
                        user.Active = Int32.Parse(value);
                        break;
                    case "Parent":
                        user.Parent = Int32.Parse(value);
                        break;
                    case "ClientId":
                        user.ClientID = value;
                        break;
                }

                userDB.SaveChanges();
            }
            catch (System.ServiceModel.FaultException ex)
            {
                var DB = new DataContext();
                var error = new Error();
                string str = "user: " + pk.ToString() + ", field: " + name.ToString();
                str += ", value: " + value + ", comment: " + comment + ", edit_date: " + DateTime.Now.ToString("dd.MM.yyyy");
                error.Command = "Admin editable " + str;
                error.Text = ex.Message;
                error.Date = DateTime.Now;
                DB.Errors.Add(error);
                DB.SaveChanges();

                return new HttpStatusCodeResult(400);
            }

            return new HttpStatusCodeResult(200);
        }

    }
  
    public class AdminControllerUserModel
    {
        public string UserName { get; set; }
        public string Fullname { get; set; }
        public string Roles { get; set; }
        public int Active { get; set; }
        public int UserId { get; set; }
        public int Parent { get; set; }
        public string Schools { get; set; }
        public string Units { get; set; }
        public string ClientId { get; set; }
    }

    public class xEditableItem
    {
        public string value { get; set; }
        public string text { get; set; }
    }
}
