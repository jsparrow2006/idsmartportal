﻿using System.Web.Mvc;
using WebSite.Filters;

namespace WebSite.Controllers
{
    [Authorize]
    [InitializeSimpleMembership]
    public class UserController : Controller
    {
        //
        // GET: /User/

        public ActionResult Index()
        {
            return null;
        }

        public ActionResult UserMenu()
        {
            return PartialView(Session["user"]);
        }
    }
}
