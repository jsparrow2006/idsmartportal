﻿using System;
using System.Linq;
using System.Web;
using WebMatrix.WebData;
using WebSite.Models;

namespace WebSite.Helpers
{
    public class ControllerHelper
    {
        private static readonly UsersContext bd = new UsersContext();

        public static bool CheckSession(HttpSessionStateBase session, int userID)
        {
            if (session["user"] == null)
            {
                var user = bd.UserProfiles.FirstOrDefault(s => s.UserId == userID);
                if (user == null)
                {
                    //throw new InvalidOperationException("Пользователь не найден.");
                    return false;
                }
                
                var result = WebApiConfig.WebClient.GetProfile(user.UserId);
                if (result.IsError)
                    return false;

                session["user"] = result.Result;
                
            }

            return true;
        }

        public static bool InitializeSession(HttpSessionStateBase session, string userName)
        {
            /*
            var userBD = bd.UserProfiles.FirstOrDefault(s => s.UserName == userName);
            if (userBD == null)
                return false;
            var user = WebApiConfig.WebClient.GetProfile(userBD.UserId);
            if (user.IsError)
                return false;
            session["user"] = user.Result;
             */
            return true;
        }
    }
}