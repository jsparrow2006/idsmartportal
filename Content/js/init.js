﻿$(function () {

    $('.slider').mobilyslider({
        content: '.sliderContent',
        children: 'div',
        transition: 'fade',
        animationSpeed: 800,
        autoplay: true,
        autoplaySpeed: 8000,
        pauseOnHover: false,
        bullets: true,
        arrows: true,
        arrowsHide: false,
        //prev: 'prev',
        //next: 'next',
        animationStart: function () { },
        animationComplete: function () { }
    });

});