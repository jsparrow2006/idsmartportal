﻿var dnevnik = {
    curr_date: new Date(),

    next_week: function () {
        this.curr_date = moment().add('w', 1);
        return this.curr_date;
    },
    prev_week: function () {
        this.curr_date = moment().subtract('w', 1);
        return this.curr_date;
    }
};