﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web.Http;
using WebSite.WebServiceReference;
using WebSite.WebServiceReference2;

namespace WebSite
{    
    public static class WebApiConfig
    {
        public static DWebServiceClient WebClient;

        public static void Register(HttpConfiguration config)
        {
            WebClient = new DWebServiceClient();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
    
    public static class WebApiConfig2
    {
        public static WebServiceSitePortTypeClient WebClient;

        
        public static void Register(HttpConfiguration config)
        //public static void Register()
        {
            var binding = new BasicHttpBinding
            {
                MaxReceivedMessageSize = int.MaxValue,
                SendTimeout = TimeSpan.FromMinutes(5),
                Security =
                {
                    Mode = BasicHttpSecurityMode.TransportCredentialOnly,
                    Transport = { ClientCredentialType = HttpClientCredentialType.Basic },
                    Message = { ClientCredentialType = BasicHttpMessageCredentialType.UserName }
                }
            };

            var client = new WebServiceSitePortTypeClient(binding,
                //new EndpointAddress("http://idsmart2011.ddns.net:9003/School/ws/WebServiceSite"));
                new EndpointAddress("http://178.124.201.36:9003/School/ws/WebServiceSite"));

            if (client.ClientCredentials != null)
            {
                client.ClientCredentials.UserName.UserName = "web";
                client.ClientCredentials.UserName.Password = "web22@";
            }

            WebClient = client;
            /*
            config.Routes.MapHttpRoute(
                name: "DefaultApi2",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            */
        }
    }    
}
