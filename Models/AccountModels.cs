﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Globalization;
using System.Web.Security;

namespace WebSite.Models
{
    public class UsersContext : DbContext
    {
        public UsersContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DataContext>());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataContext>());
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<Pupil> Pupils { get; set; }
        public DbSet<Child> Children { get; set; }
        /*
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<UserProfile>()
                .HasMany(c => c.Children).WithRequired(c => c.UserProfile);            
        }
         */
    }

    [Table("UserProfile")]
    public class UserProfile
    {
        [Key]
        [DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Fullname_1 { get; set; }
        public string Fullname_2 { get; set; }
        public string Fullname_3 { get; set; }
        public DateTime Birthdate { get; set; }
        public int Parent { get; set; }
        public string SchoolID { get; set; }
        public string Units { get; set; }
        public int Code { get; set; }
        public int Active { get; set; }
        public string ClientID { get; set; }
        public virtual ICollection<Child> Children { get; set; }

        public string fullname
        {
            get
            { return Fullname_1 + " " + Fullname_2 + " " + Fullname_3; }
        }

    }

    [Table("Children")]
    public class Child
    {
        [Key] [Column(Order = 0)]
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserProfile UserProfile { get; set; }

        [Key] [Column(Order = 1)]
        public int Pupil_ID { get; set; }
        [ForeignKey("Pupil_ID")]
        public virtual Pupil Pupil { get; set; }
    }

    [Table("Pupils")]
    public class Pupil
    {
        [Key]
        public int Pupil_ID { get; set; }
        // public int Parent1_ID { get; set; }
        // public int Parent2_ID { get; set; }
        public int School_ID { get; set; }
        public string School_Name { get; set; }
        public int Unit_ID { get; set; }
        public string Class_Name { get; set; }
        public string Fullname_1 { get; set; }
        public string Fullname_2 { get; set; }
        public string Fullname_3 { get; set; }
        public string Card_ID { get; set; }
        public virtual ICollection<Child> Parents { get; set; }

        public string fullname
        {
            get
            { return Fullname_1 + " " + Fullname_2 + " " + Fullname_3; }
        }
    }

    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Текущий пароль")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Пароль должен содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Пароль должен содержать не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("NewPassword", ErrorMessage = "Новый пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required]
        [Display(Name = "Имя пользователя")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }

    public class RegisterModel
    {
        [Required(ErrorMessage = "Заполните имя пользователя")]
        [Display(Name = "Имя пользователя")]
        [RegularExpression(@"^[а-яёА-ЯЁa-zA-Z0-9\-_]+$", ErrorMessage = "Имя пользователя: Вводите только русские или латинские буквы")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Заполните поле 'пароль'")]
        [StringLength(100, ErrorMessage = "Пароль должен содержать не менее {2} символов.", MinimumLength = 5)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        [Compare("Password", ErrorMessage = "Пароль и его подтверждение не совпадают.")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessage = "Заполните поле 'Email'")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Введите корректный Email.")]
        [Display(Name = "Email пользователя")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Заполните поле 'телефон'")]
        [StringLength(9, ErrorMessage = "Телефон должен содержать {2} цифр ровно.", MinimumLength = 9)]
        [Display(Name = "Мобильный телефон")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "Телефон: Вводите только цифры")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Заполните поле 'фамилия'")]
        [Display(Name = "Фамилия")]
        [RegularExpression(@"^[а-яёА-ЯЁ\-]+$", ErrorMessage = "Фамилия: Вводите только русские буквы")]
        public string Fullname_1 { get; set; }

        [Required(ErrorMessage = "Заполните поле 'имя'")]
        [Display(Name = "Имя")]
        [RegularExpression(@"^[а-яёА-ЯЁ\-]+$", ErrorMessage = "Имя: Вводите только русские буквы")]
        public string Fullname_2 { get; set; }

        [Required(ErrorMessage = "Заполните поле 'отчество'")]
        [Display(Name = "Отчество")]
        [RegularExpression(@"^[а-яёА-ЯЁ\-]+$", ErrorMessage = "Отчество: Вводите только русские буквы")]
        public string Fullname_3 { get; set; }

        [Required(ErrorMessage = "Заполните поле 'дата рождения'")]
        [DataType(DataType.Date)]
        [Display(Name = "Дата рождения")]
        public DateTime Birthdate { get; set; }

        /*
        public IEnumerable<ParentSel> ParentSelect =
            new List<ParentSel>
            {
                new { value = 1, text = "Родителем" },
                new { value = 0, text = "Учителем" },
            };
        */
        [Required]
        [Display(Name = "Вы являетесь:")]
        public int Parent { get; set; }

        [Display(Name = "Школа")]
        public string School_ID { get; set; }

    }

    public class SetChildModel
    {
        [Required]
        [Display(Name = "Школа")]
        public int School_ID { get; set; }

        [Required]
        [Display(Name = "Класс")]
        public int Unit_ID { get; set; }
        
        /*
        [Required]
        [Display(Name = "Класс")]
        [StringLength(3, ErrorMessage = "Класс должен содержать цифру и букву", MinimumLength = 2)]
        [RegularExpression(@"^[а-яА-Я0-9]+$", ErrorMessage = "Класс: Вводите только русские буквы и цифры")]
        public string Class_Name { get; set; }
        */
        [Required(ErrorMessage = "Заполните поле 'фамилия'")]
        [Display(Name = "Фамилия")]
        [RegularExpression(@"^[а-яёА-ЯЁ\-]+$", ErrorMessage = "Фамилия: Вводите только русские буквы")]
        public string Fullname_1 { get; set; }

        [Required(ErrorMessage = "Заполните поле 'имя'")]
        [Display(Name = "Имя")]
        [RegularExpression(@"^[а-яёА-ЯЁ\-]+$", ErrorMessage = "Имя: Вводите только русские буквы")]
        public string Fullname_2 { get; set; }

        [Required(ErrorMessage = "Заполните поле 'отчество'")]
        [Display(Name = "Отчество")]
        [RegularExpression(@"^[а-яёА-ЯЁ\-]+$", ErrorMessage = "Отчество: Вводите только русские буквы")]
        public string Fullname_3 { get; set; }

        [Required(ErrorMessage = "Заполните поле 'номер карты'")]
        [StringLength(9, ErrorMessage = "Номер должен содержать 8 цифр, разделенных точкой", MinimumLength = 9)]
        [RegularExpression(@"^[0-9.]+$", ErrorMessage = "Номер: Вводите только цифры и точку")]
        [Display(Name = "Номер карты")]
        public string Card_ID { get; set; }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
    /*
    public class ParentSel
    {
        public int id { get; set; }
        public string text { get; set; }
    }
    */
}
