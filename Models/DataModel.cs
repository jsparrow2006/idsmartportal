﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using WebMatrix.WebData;

namespace WebSite.Models
{
    public class BaseModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
    }

    public class GoodsModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public int Months { get; set; }
    }

    public class DataContext : DbContext
    {
        public DataContext()
            : base("IdsDataConnection")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DataContext>());
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DataContext>());
        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Text> Texts { get; set; }
        public DbSet<News> News { get; set; }
        public DbSet<Error> Errors { get; set; }
        public DbSet<Login_log> Login_logs { get; set; }

        /*
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Child>()
            .HasRequired(p => p.UserProfile).WithMany(p => p.Children); //.Map(p => p.MapKey("ParentId"));

            modelBuilder.Entity<Child>()
            .HasRequired(p => p.Pupil).WithMany(p => p.Parents); //.Map(p => p.MapKey("ID"));
            
        }
        */
    }

    [Table("Orders")]
    public class Order
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public int UserId { get; set; }
        //[Required]
        //public int Pupil_id { get; set; }
        [Required]
        public string phone { get; set; }
        [Required]
        public string email { get; set; }
        public string timestamp { get; set; }
        public string currency_id { get; set; }
        public string amount { get; set; }
        public string method { get; set; }
        public string order_id { get; set; }
        public string transaction_id { get; set; }
        public string payment_type { get; set; }
        public string rrn { get; set; }
        [Required]
        public DateTime date { get; set; }
        public string goods { get; set; }        
    }

    [Table("Message")]
    public class Message
    {
        [Key]
        public int ID { get; set; }
        [Required]
        [StringLength(20)]
        [Display(Name = "Имя")]
        public string Name { get; set; }
        [Required]
        [StringLength(30)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Сообщение")]
        public string Body { get; set; }
        public DateTime Date { get; set; }
        [StringLength(15)]
        [Display(Name = "Телефон")]
        public string Phone { get; set; }
    }

    [Table("Texts")]
    public class Text
    {
        [Key]
        public int ID { get; set; }
        [Required]
        public string Page { get; set; }
        [Required]
        public string Element { get; set; }
        public string Value { get; set; }
    }

    [Table("News")]
    public class News
    {
        [Key]
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public int Active { get; set; }
    }

    [Table("Errors")]
    public class Error
    {
        [Key]
        public int ID { get; set; }
        public string Command { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }

    [Table("Login_logs")]
    public class Login_log
    {
        [Key]
        public int log_id { get; set; }
        public string username { get; set; }
        public DateTime log_date { get; set; }
    }

    //[Table("Clients")]
    //public class Client
    //{
    //    [Key]
    //    public int ID { get; set; }
    //    public string Name { get; set; }
    //    public int UserID { get; set; }
    //    public virtual ICollection<Pupil> Pupils { get; set; }
    //    public virtual ICollection<ClientTarif> Tarifs { get; set; }
    //}

    //[Table("Pupils")]
    //public class Pupil
    //{
    //    [Key]
    //    public int ID { get; set; }
    //    public string Name { get; set; }
    //    public virtual Unit Unit { get; set; }
    //    public virtual School School { get; set; }
    //    public virtual Client Client { get; set; }
    //}

    //[Table("Tarifs")]
    //public class Tarif
    //{
    //    [Key]
    //    public int ID { get; set; }
    //    public string Name { get; set; }
    //}

    //[Table("ClientTarifs")]
    //public class ClientTarif
    //{
    //    [Key]
    //    public int ID { get; set; }
    //    public virtual Client Client { get; set; }
    //    public virtual Tarif Tarif { get; set; }
    //    public DateTime DateLost { get; set; }
    //}

    //[Table("Units")]
    //public class Unit
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    //    public int ID { get; set; }
    //    public string Name { get; set; }
    //}

    //[Table("Schools")]
    //public class School
    //{
    //    [Key]
    //    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    //    public int ID { get; set; }
    //    public string Name { get; set; }
    //}
}